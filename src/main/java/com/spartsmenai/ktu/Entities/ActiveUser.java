package com.spartsmenai.ktu.Entities;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import sun.java2d.loops.GeneralRenderer;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Document(collection = "activeuser")
@Entity
public class ActiveUser {
	
	@Id
	private String id;
	private String username;
	private String password;
	private Integer user_level;
	private String email;
	private String ip;
	//private Date creation_date_of_account;
	private String originalid;	
	private String imonesKod;
	private String organizacpav;
	private String organizacadr;
	private String Telnr;
	 
	public String getImonesKod() {
		return imonesKod;
	}

	public void setImonesKod(String imonesKod) {
		this.imonesKod = imonesKod;
	}

	public String getOrganizacpav() {
		return organizacpav;
	}

	public void setOrganizacpav(String organizacpav) {
		this.organizacpav = organizacpav;
	}

	public String getOrganizacadr() {
		return organizacadr;
	}

	public void setOrganizacadr(String organizacadr) {
		this.organizacadr = organizacadr;
	}

	public String getTelnr() {
		return Telnr;
	}

	public void setTelnr(String telnr) {
		Telnr = telnr;
	}

	public String getOriginalid() {
		return originalid;
	}

	public void setOriginalid(String originalid) {
		this.originalid = originalid;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public ActiveUser(String username, String password, Integer user_level,
			String email,String originalid, String ip, String Telnr) {

		this.username = username;
		this.password = password;
		this.user_level = user_level;
		this.email = email;
		this.originalid = originalid;
		this.ip = ip;
		this.Telnr = Telnr;
		
		//this.creation_date_of_account = creation_date_of_account;
	}
	public ActiveUser(String username, String password, Integer user_level,
			String email,String originalid, String ip, String Telnr, 
			String imonesKod,String organizacpav, String organizacadr) {

		this.username = username;
		this.password = password;
		this.user_level = user_level;
		this.email = email;
		this.originalid = originalid;
		this.ip = ip;
		this.Telnr = Telnr;
		this.imonesKod = imonesKod;
		this.organizacpav = organizacpav;
		this.organizacadr = organizacadr;
		//this.creation_date_of_account = creation_date_of_account;
	}
	public ActiveUser() {}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getUser_level() {
		return user_level;
	}
	public void setUser_level(Integer user_level) {
		this.user_level = user_level;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	/*public Date getCreation_date_of_account() {
		return creation_date_of_account;
	}
	public void setCreation_date_of_account(Date creation_date_of_account) {
		this.creation_date_of_account = creation_date_of_account;
	}
	 */
	 
	 
}

package com.spartsmenai.ktu.Entities;


import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.Date;

@Document(collection = "adverts")
@Entity
public class Advert {

    @Id
    /*@GeneratedValue(strategy = GenerationType.AUTO)*/
    private String id;
    private String authorId;
    private String title;
    private String description;
    private String place;
    private String imageLink;
    private int max_participants;
    private Date event_date;
    private Date creation_date;
    private Date update_date;

    public Advert(String id, String authorId, String title, String description, String place, String imageLink,
                  int max_participants, Date event_date, Date creation_date, Date update_date) {
        this.id = id;
        this.authorId = authorId;
        this.title = title;
        this.description = description;
        this.place = place;
        this.imageLink = imageLink;
        this.max_participants = max_participants;
        this.event_date = event_date;
        this.creation_date = creation_date;
        this.update_date = update_date;
    }

    public Advert(){ }

    public String getAuthorId() { return authorId; }

    public void setAuthorId(String authorId) { this.authorId = authorId; }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public String getPlace() { return place; }

    public void setPlace(String place) { this.place = place; }

    public int getMax_participants() { return max_participants; }

    public void setMax_participants(int max_participants) { this.max_participants = max_participants; }

    public Date getEvent_date() { return event_date; }

    public void setEvent_date(Date event_date) { this.event_date = event_date; }

    public Date getCreation_date() { return creation_date; }

    public void setCreation_date(Date creation_date) { this.creation_date = creation_date; }

    public Date getUpdate_date() { return update_date; }

    public void setUpdate_date(Date update_date) { this.update_date = update_date; }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

}
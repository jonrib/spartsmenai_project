package com.spartsmenai.ktu.Entities;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Repository;

import javax.persistence.Entity;
import javax.persistence.Id;


@Document(collection = "advertcategory")
@Entity
public class AdvertCategory {
    @Id
    private String id;

    private String categoryName;
    private String advertId;


    public AdvertCategory(String id, String categoryName, String advertId) {
        this.id = id;
        this.categoryName = categoryName;
        this.advertId = advertId;
    }

    public AdvertCategory() {}

    public AdvertCategory(String categoryName, String advertId) {
        this.categoryName = categoryName;
        this.advertId = advertId;
    }

    public String getAdvertId() {
        return advertId;
    }

    public void setAdvertId(String advertId) {
        this.advertId = advertId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

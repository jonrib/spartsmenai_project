package com.spartsmenai.ktu.Entities;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Document(collection = "advertlikes")
@Entity
public class AdvertLike {
    @Id
    private String id;
    private String advertId;
    private String userId;
    private Date dateOfLike;

    public AdvertLike(String id, String ardvertId, String userId, Date dateOfLike) {
        this.id = id;
        this.advertId = ardvertId;
        this.userId = userId;
        this.dateOfLike = dateOfLike;
    }

    public AdvertLike(){}

    public Date getDateOfLike() {
        return dateOfLike;
    }

    public void setDateOfLike(Date dateOfMark) {
        this.dateOfLike = dateOfMark;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAdvertId() {
        return advertId;
    }

    public void setAdvertId(String advertId) {
        this.advertId = advertId;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

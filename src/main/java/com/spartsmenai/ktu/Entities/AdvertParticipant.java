package com.spartsmenai.ktu.Entities;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Document(collection = "advertparticipants")
@Entity
public class AdvertParticipant {

    @Id
    private String id;
    private String advertId;
    private String participantId;
    private Date registerDate;

    public AdvertParticipant(String id, String advertId, String participantId, Date registerDate) {
        this.id = id;
        this.advertId = advertId;
        this.participantId = participantId;
        this.registerDate = registerDate;
    }
    public AdvertParticipant() {}


    public String getAdvertId() {
        return advertId;
    }

    public void setAdvertId(String advertId) {
        this.advertId = advertId;
    }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

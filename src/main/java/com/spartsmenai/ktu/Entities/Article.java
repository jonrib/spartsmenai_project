package com.spartsmenai.ktu.Entities;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import sun.java2d.loops.GeneralRenderer;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.util.ArrayList;
import java.util.Date;

@Document(collection = "articles")
@Entity
public class Article {

    @Id
    /*@GeneratedValue(strategy = GenerationType.AUTO)*/
    private String id;
    private String name;
    private  String tags;
    @Indexed(direction = IndexDirection.ASCENDING)
    private Date creation_date;
    private Date update_date;
    private String imageLink;
    private String sourceLink;
    private String content;
    private ArrayList<Category> category;

    public Article(String name, String tags, Date creation_date, Date update_date, String imageLink, String sourceLink, String content) {
        this.name = name;
        this.tags = tags;
        this.creation_date = creation_date;
        this.update_date = update_date;
        this.imageLink = imageLink;
        this.sourceLink = sourceLink;
        this.content = content;
        //this.category = category;
    }
    
    public Article() {}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public Date getCreation_date() {
		return creation_date;
	}

	public void setCreation_date(Date string) {
		this.creation_date = string;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

	public String getImageLink() {
		return imageLink;
	}

	public void setImageLink(String imageLink) {
		this.imageLink = imageLink;
	}

	public String getSourceLink() {
		return sourceLink;
	}

	public void setSourceLink(String sourceLink) {
		this.sourceLink = sourceLink;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public ArrayList<Category> getCategory() {
		return category;
	}

	public void setCategory(ArrayList<Category> category) {
		this.category = category;
	}

	public void setCategoryName(Category category) {
		if (this.category == null)
			this.category = new ArrayList<Category>();
		this.category.add(category);
	}

}

package com.spartsmenai.ktu.Entities;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import sun.java2d.loops.GeneralRenderer;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Document(collection = "categories")
@Entity
public class Category {

    @Id
    /*@GeneratedValue(strategy = GenerationType.AUTO)*/
    private String id;
    private String name;

    public Category(String name) {
        this.name = name;
    }
    
    public Category() {}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

package com.spartsmenai.ktu.Entities;

import java.util.Date;

import javax.persistence.Id;

public class Comment {
	@Id
	private String id;
	private String commentText;
	private Date commentDate;
	private Date editDate;
	private User user;
	
	public Comment(String commentText, Date commentDate, Date editDate, User user) {
		this.commentText = commentText;
		this.commentDate = commentDate;
		this.editDate = editDate;
		this.user = user;
	}
	
	public Comment() {
		
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	public Date getCommentDate() {
		return commentDate;
	}
	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}
	public Date getEditDate() {
		return editDate;
	}
	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	

}

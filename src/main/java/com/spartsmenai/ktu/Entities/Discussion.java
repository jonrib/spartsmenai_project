package com.spartsmenai.ktu.Entities;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import sun.java2d.loops.GeneralRenderer;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document(collection = "discussions")
@Entity
public class Discussion {

    @Id
    /*@GeneratedValue(strategy = GenerationType.AUTO)*/
    private String id;
    private String topic;
    private  String description;
    private Date creation_date;
    private Date update_date;

    private ArrayList<User> user;
    private ArrayList<Comment> comments;

    public Discussion(String topic, String description, Date creation_date, Date update_date, ArrayList<Comment> comments, ArrayList<User> user) {
    	this.topic = topic;
    	this.description = description;
        this.creation_date = creation_date;
        this.update_date = update_date;
        this.comments = comments;
        this.user = user;
    }
    
    public void addComment(Comment comment) {
    	comments.add(comment);
    }
    
    public void removeComment(String id) {
    	Comment toRemove = null;
    	
    	for (Comment comment : comments) {
    		if (comment.getId().equals(id)) {
    			toRemove = comment;
    		}
    	}
    	
    	if (toRemove != null)
    		comments.remove(toRemove);
    }
    
    public Discussion() {
    	comments = new ArrayList<Comment>();
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreation_date() {
		return creation_date;
	}

	public void setCreation_date(Date creation_date) {
		this.creation_date = creation_date;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(ArrayList<Comment> comments) {
		this.comments = comments;
	}

	public ArrayList<User> getUser() {
		return user;
	}

	public void setUser(ArrayList<User> user) {
		this.user = user;
	}

	public void addUser(User user) {
		this.user = new ArrayList<>();
		this.user.add(user);
	}

	
}

package com.spartsmenai.ktu.Entities;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import sun.java2d.loops.GeneralRenderer;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.Date;

@Document(collection = "games")
@Entity
public class Game {

    @Id
    /*@GeneratedValue(strategy = GenerationType.AUTO)*/
    private String id;
    private String name;
    private String description;
    private String photo;
    private String place;
    private Date event_date;
    private String team_a;
    private String team_b;
    private Integer result_a;
    private Integer result_b;
    private Date creation_date;
    private Date update_date;
    private ArrayList<Category> category;
    private ArrayList<User> acceptedusers;
//turi comment entity list'a

    public Game(String id, String name, String description, String photo, String place, Date event_date, String team_a, String team_b, Integer result_a, Integer result_b, Date creation_date, Date update_date) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.photo = photo;
        this.place = place;
        this.event_date = event_date;
        this.team_a = team_a;
        this.team_b = team_b;
        this.result_a = result_a;
        this.result_b = result_b;
        this.creation_date = creation_date;
        this.update_date = update_date;
    }

    public Game() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public ArrayList<Category> getCategory() {
		return category;
	}

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Date getEvent_date() {
        return event_date;
    }

    public void setEvent_date(Date event_date) {
        this.event_date = event_date;
    }

    public String getTeam_a() {
        return team_a;
    }

    public void setTeam_a(String team_a) {
        this.team_a = team_a;
    }

    public String getTeam_b() {
        return team_b;
    }

    public void setTeam_b(String team_b) {
        this.team_b = team_b;
    }

    public Integer getResult_a() {
        return result_a;
    }

    public void setResult_a(Integer result_a) {
        this.result_a = result_a;
    }

    public Integer getResult_b() {
        return result_b;
    }

    public void setResult_b(Integer result_b) {
        this.result_b = result_b;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public void setCategoryName(Category category) {
        if (this.category == null)
            this.category = new ArrayList<Category>();
        this.category.add(category);
    }
    
    public void setActiveUser(User acceptedusers) {
        if (this.acceptedusers == null)
            this.acceptedusers = new ArrayList<User>();
        System.out.println(acceptedusers.getUsername());
        System.out.println(this.acceptedusers.size());
        this.acceptedusers.add(acceptedusers);
        System.out.println(this.acceptedusers.size());
    }
    public ArrayList<User> getActiveUser() {
		return acceptedusers;
	}
}
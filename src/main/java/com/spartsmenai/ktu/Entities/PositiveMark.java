package com.spartsmenai.ktu.Entities;

import java.util.Date;

import javax.persistence.Id;

public class PositiveMark {
	@Id
	private String id;
	private String ip;
	private Date dateOfMark;
	
	
	public PositiveMark(String ip, Date dateOfMark) {
		this.ip = ip;
		this.dateOfMark = dateOfMark;
	}
	
	public PositiveMark() {
		
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Date getDateOfMark() {
		return dateOfMark;
	}
	public void setDateOfMark(Date dateOfMark) {
		this.dateOfMark = dateOfMark;
	}
	

}

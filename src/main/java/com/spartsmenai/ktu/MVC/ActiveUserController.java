package com.spartsmenai.ktu.MVC;

import com.mongodb.util.JSON;
import com.spartsmenai.ktu.Entities.ActiveUser;
import com.spartsmenai.ktu.Entities.User;
import com.spartsmenai.ktu.Repositories.ActiveUserRepository;
import com.spartsmenai.ktu.Repositories.UserRepository;
import com.spartsmenai.ktu.Services.ArticleService;
import com.spartsmenai.ktu.Services.UserService;
import com.spartsmenai.ktu.Services.ActiveUserService; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/activeuser")
public class ActiveUserController {

	  @Autowired
	    private ActiveUserService activeuserService;

	    @Autowired
	    private ActiveUserRepository activeuserRepository;
	    
	    @Autowired
	    private UserService userService;

	    @Autowired
	    private UserRepository userRepository;
	    
	    
	    private List<ActiveUser> allUsers = new ArrayList<ActiveUser>();
	    
	    @RequestMapping(method = RequestMethod.GET)
	    public List<ActiveUser> getAllUsers(){
	        return activeuserService.getAllUsers();
	    }

	    /*@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	    public  Article getArticleById(@PathVariable("id") int id){
	        return articleService.getArticleById(id);
	    }*/
	    
	    @GetMapping(value="/findById")
	    public ActiveUser findUser(@RequestParam("id") String userId){
	    	//System.out.println(JSON.serialize(articleRepository.findOne(articleId)));
	    	//return JSON.serialize(articleRepository.findOne(articleId));
	    	return activeuserRepository.findOne(userId);
	    }
	    
	    
	    @GetMapping(value="/findactiveuser")
	    public ActiveUser findactiveuser(HttpServletRequest request){
	    	ActiveUser user = new ActiveUser();
	    	List<ActiveUser> allActive = activeuserRepository.findAll();
			for (ActiveUser usertemp : allActive) {
				
				if (usertemp.getIp().equals(request.getRemoteAddr())) {
					user = usertemp;
				}
				
			}
	    	return user;
	    }
	    
	    @RequestMapping(value="/logout")
	    public void editUserProfile( HttpServletRequest request) {
	    	
	    	
	    	List<ActiveUser> allActive = activeuserRepository.findAll();
			for (ActiveUser user : allActive) {
				
				if (user.getIp().equals(request.getRemoteAddr())) {
					activeuserRepository.delete(user);
				}
			}
	    }
	    
	    @PostMapping(value="/edituser")
	 		public String editUser(@RequestParam("username") String username, @RequestParam("oldpass") String oldpass,
	 				@RequestParam("pass") String pass, @RequestParam("email") String email,
	 				@RequestParam("telnr") String telnr, HttpServletRequest request){
	 	    				
	 			List<ActiveUser> allActive = activeuserRepository.findAll();
	 			
				for (ActiveUser user : allActive) {
					if (user.getIp().equals(request.getRemoteAddr())) {
						

						List<User> allUsers = userRepository.findAll();
			 			for (User userpap : allUsers) {
			 				if (userpap.getUsername().equals(user.getUsername()) && user.getPassword().equals(oldpass)) {
			 					userpap.setEmail(email);
			 					userpap.setPassword(pass);
			 					userpap.setUsername(username);
			 					userpap.setTelnr(telnr);
			 					userRepository.save(userpap);
			 				}
			 			}
			 			
			 			user.setEmail(email);
						user.setPassword(pass);
						user.setUsername(username);
						user.setTelnr(telnr);
						
						activeuserRepository.save(user);
					}
					
				}
				return "success";
	 		}
	    
	    
	    @PostMapping(value="/editorganizat")
 		public String editorganisat(@RequestParam("username") String username, @RequestParam("oldpass") String oldpass,
 				@RequestParam("pass") String pass, @RequestParam("email") String email,
 				@RequestParam("telnr") String telnr, @RequestParam("imoneskod") String imonesKod, 
 				@RequestParam("organizacpav") String organizacpav, @RequestParam("organizacadre") String organizacadr, 
 				HttpServletRequest request){
 	    				
 			List<ActiveUser> allActive = activeuserRepository.findAll();
 			
			for (ActiveUser user : allActive) {
				if (user.getIp().equals(request.getRemoteAddr())) {
					

					List<User> allUsers = userRepository.findAll();
		 			for (User userpap : allUsers) {
		 				if (userpap.getUsername().equals(user.getUsername()) && user.getPassword().equals(oldpass)) {
		 					userpap.setEmail(email);
		 					userpap.setPassword(pass);
		 					userpap.setUsername(username);
		 					userpap.setTelnr(telnr);
		 					userpap.setImonesKod(imonesKod);
		 					userpap.setOrganizacpav(organizacpav);
		 					userpap.setOrganizacadr(organizacadr);
		 					userRepository.save(userpap);
		 				}
		 			}
		 			
		 			user.setEmail(email);
					user.setPassword(pass);
					user.setUsername(username);
					user.setTelnr(telnr);
					user.setImonesKod(imonesKod);
 					user.setOrganizacpav(organizacpav);
 					user.setOrganizacadr(organizacadr);
					activeuserRepository.save(user);
				}
				
			}
			return "success";
 		}
	    
	    
	    @PostMapping(value="/save")
		public String editUserProfile(@RequestParam("username") String username, @RequestParam("pass") String pass, HttpServletRequest request){
	    	List<User> allUsers = userRepository.findAll();
	    	User userE = null;
			for (User user : allUsers) {
				if (user.getUsername().equals(username) && user.getPassword().equals(pass)) {
					userE = user;
				}
				
			}
			ActiveUser active;
			
			if(userE.getUser_level() == 4)
			{
				active = new ActiveUser(userE.getUsername(), userE.getPassword(), userE.getUser_level(), userE.getEmail(),userE.getId(), request.getRemoteAddr(), userE.getTelnr());
			}
			else {
				active = new ActiveUser(userE.getUsername(), userE.getPassword(), userE.getUser_level(), userE.getEmail(),userE.getId(), request.getRemoteAddr(), userE.getTelnr(), userE.getImonesKod(), userE.getOrganizacpav(), userE.getOrganizacadr());
			}
			
			List<ActiveUser> allActive = activeuserRepository.findAll();
			for (ActiveUser user : allActive) {
				if (user.getIp().equals(request.getRemoteAddr())) {
					activeuserRepository.delete(user);
				}
				
			}
			activeuserRepository.save(active);
			
			return "Post Successfully!";
		}
	
	
}

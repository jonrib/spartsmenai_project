package com.spartsmenai.ktu.MVC;

import com.spartsmenai.ktu.Entities.Advert;
import com.spartsmenai.ktu.Entities.AdvertCategory;
import com.spartsmenai.ktu.Entities.AdvertLike;
import com.spartsmenai.ktu.Entities.AdvertParticipant;
import com.spartsmenai.ktu.Repositories.AdvertCategoryRepository;
import com.spartsmenai.ktu.Repositories.AdvertLikeRepository;
import com.spartsmenai.ktu.Repositories.AdvertParticipantRepository;
import com.spartsmenai.ktu.Repositories.AdvertRepository;
import com.spartsmenai.ktu.Services.AdvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/adverts")
public class AdvertisementsController {
    @Autowired
    private AdvertService advertService;

    @Autowired
    private AdvertCategoryRepository advertCategoryRepository;

    @Autowired
    private AdvertParticipantRepository advertParticipantRepository;

    @Autowired
    private AdvertLikeRepository advertLikeRepository;

    @Autowired
    private AdvertRepository advertRepository;

    private List<Advert> allAdverts = new ArrayList<Advert>();


    @RequestMapping(method = RequestMethod.GET)
    public List<Advert> getAllAdverts() {return advertService.getAllAdverts();}

    @RequestMapping(value="/getAdvertsSortedByCreationDateAsc")
    public List<Advert> getAdvertsSortedByCreationDateAsc() {
        List<Advert> adverts = advertRepository.findAll();
        Collections.sort(adverts, new SortByCreationDateAsc());
        return adverts;
    }

    class SortByCreationDateAsc implements Comparator<Advert>
    {
        // Used for sorting in ascending order of
        public int compare(Advert a, Advert b)
        {
            return a.getCreation_date().compareTo(b.getCreation_date());
        }
    }

    @RequestMapping(value="/getAdvertsSortedByCreationDateDesc")
    public List<Advert> getAdvertsSortedByCreationDateDesc() {
        List<Advert> adverts = advertRepository.findAll();
        Collections.sort(adverts, new SortByCreationDateDesc());
        return adverts;
    }

    class SortByCreationDateDesc implements Comparator<Advert>
    {
        // Used for sorting in ascending order of
        public int compare(Advert a, Advert b)
        {
            return -1*a.getCreation_date().compareTo(b.getCreation_date());
        }
    }

    @RequestMapping(value="/getAdvertsSortedByTitleAsc")
    public List<Advert> getAdvertsSortedByTitleAsc() {
        List<Advert> adverts = advertRepository.findAll();
        Collections.sort(adverts, new SortByTitleAsc());
        return adverts;
    }

    class SortByTitleAsc implements Comparator<Advert>
    {
        // Used for sorting in ascending order of
        public int compare(Advert a, Advert b)
        {
            return a.getTitle().compareTo(b.getTitle());
        }
    }

    @RequestMapping(value="/getAdvertsSortedByTitleDesc")
    public List<Advert> getAdvertsSortedByTitleDesc() {
        List<Advert> adverts = advertRepository.findAll();
        Collections.sort(adverts, new SortByTitleDesc());
        return adverts;
    }

    class SortByTitleDesc implements Comparator<Advert>
    {
        // Used for sorting in desending order of
        public int compare(Advert a, Advert b)
        {
            return -1*a.getTitle().compareTo(b.getTitle());
        }
    }

    @RequestMapping(value="/getAdvertsSortedByEventDateAsc")
    public List<Advert> getAdvertsSortedByEventDateAsc() {
        List<Advert> adverts = advertRepository.findAll();
        Collections.sort(adverts, new SortByEventDateAsc());
        return adverts;
    }

    class SortByEventDateAsc implements Comparator<Advert>
    {
        // Used for sorting in ascending order of
        public int compare(Advert a, Advert b)
        {
            return a.getEvent_date().compareTo(b.getEvent_date());
        }
    }

    @RequestMapping(value="/getAdvertsSortedByEventDateDesc")
    public List<Advert> getAdvertsSortedByEventDateDesc() {
        List<Advert> adverts = advertRepository.findAll();
        Collections.sort(adverts, new SortByEventDateDesc());
        return adverts;
    }

    class SortByEventDateDesc implements Comparator<Advert>
    {
        // Used for sorting in ascending order of
        public int compare(Advert a, Advert b)
        {
            return -1*a.getEvent_date().compareTo(b.getEvent_date());
        }
    }

    //@RequestMapping(value = "/advert")
    //public String index() {
    //    return "Advert";
    //}

    @RequestMapping(value="/findById")
    public Advert findAdvert(@RequestParam("id") String advertId) {
        return advertRepository.findOne(advertId);
    }

    @PostMapping(value="/save")
    public String editAdvert(@RequestBody Advert advert, @RequestParam("id") String advertId){
        if (!advertId.equals("none") && advertId != null && !advertId.equals("")){
            Advert oldEntity = advertRepository.findOne(advertId);
            //advertRepository.delete(oldEntity);
            advert.setUpdate_date(new Date());
            advert.setId(oldEntity.getId());
            advertRepository.save(advert);
        } else {
            advert.setCreation_date(new Date());
            advert.setUpdate_date(new Date());
            advertRepository.save(advert);
        }
        return "POST SUCCESSFUL";
    }

    @PostMapping(value="/saveCategories")
    public String editCategory(@RequestBody List<AdvertCategory> categories, @RequestParam("id") String advertId){
        List<AdvertCategory> allCats = advertCategoryRepository.findAll();
        for(AdvertCategory advCat: allCats)
        {
            if(advCat.getAdvertId().equals(advertId))
                advertCategoryRepository.delete(advCat);
        }

        advertCategoryRepository.save(categories);
        return "Success";
    }

    @RequestMapping(value="/getCategoriesById")
    public List<AdvertCategory> getCategoriesById(@RequestParam("id") String advertId)
    {
        List<AdvertCategory> allCats = advertCategoryRepository.findAll();
        List<AdvertCategory> catsById = new ArrayList<AdvertCategory>();
        for(AdvertCategory cat: allCats)
        {
            if(cat.getAdvertId().equals(advertId))
                catsById.add(cat);
        }

        return catsById;
    }

    @PostMapping(value = "/findByQuery")
    public List<Advert> getAllAdvertsByFilterQuery(@RequestBody Advert searchQuery) {
        List<Advert> filtered = advertService.getAllAdverts();;
        ArrayList<Boolean> bools = new ArrayList<Boolean>();

        for(int i = 0; i < filtered.size(); i++) {
            bools.add(true);
        }

        for(int i = 0; i < filtered.size(); i++) {
            if (searchQuery.getTitle().equals("")) break;
            else if (!filtered.get(i).getTitle().contains(searchQuery.getTitle()))
                bools.set(i, false);
        }

        for(int i = 0; i < filtered.size(); i++) {
            if (searchQuery.getPlace().equals("")) break;
            else if (!filtered.get(i).getPlace().contains(searchQuery.getPlace()))
                bools.set(i, false);
        }

        for(int i = 0; i < filtered.size(); i++) {
            if(searchQuery.getMax_participants() == 0) break;

            if(filtered.get(i).getMax_participants() != searchQuery.getMax_participants())
                bools.set(i, false);
        }

        for(int i = 0; i < filtered.size(); i++) {
            Advert semiFiltered = filtered.get(i);

            //System.out.println(searchQuery.getCreation_date());
            if(searchQuery.getCreation_date() == null) {
                break;
            }

            Calendar calendar = new GregorianCalendar();
            Date data = semiFiltered.getCreation_date();
            calendar.setTime(data);
            int yearAdvert = calendar.get(Calendar.YEAR);
            int dayAdvert = calendar.get(Calendar.DAY_OF_MONTH);
            int monthAdvert = calendar.get(Calendar.MONTH);

            data = searchQuery.getCreation_date();
            calendar.setTime(data);
            int qyearAdvert = calendar.get(Calendar.YEAR);
            int qdayAdvert = calendar.get(Calendar.DAY_OF_MONTH);
            int qmonthAdvert = calendar.get(Calendar.MONTH);

            if(qyearAdvert != yearAdvert || qdayAdvert != dayAdvert || qmonthAdvert != monthAdvert) {
                bools.set(i, false);
            }
        }

        for(int i = 0; i < filtered.size(); i++) {
            Advert semiFiltered = filtered.get(i);

            if(searchQuery.getEvent_date() == null) {
                break;
            }
            System.out.println(semiFiltered.getTitle());
            Calendar calendar = new GregorianCalendar();
            Date data = semiFiltered.getEvent_date();
            calendar.setTime(data);
            int yearAdvert = calendar.get(Calendar.YEAR);
            int dayAdvert = calendar.get(Calendar.DAY_OF_MONTH);
            int monthAdvert = calendar.get(Calendar.MONTH);

            data = searchQuery.getEvent_date();
            calendar.setTime(data);
            int qyearAdvert = calendar.get(Calendar.YEAR);
            int qdayAdvert = calendar.get(Calendar.DAY_OF_MONTH);
            int qmonthAdvert = calendar.get(Calendar.MONTH);

            if(qyearAdvert != yearAdvert || qdayAdvert != dayAdvert || qmonthAdvert != monthAdvert){
                bools.set(i, false);
            }
        }

        for(int i = 0; i < filtered.size(); i++)
        {
            if (bools.get(i) == false)
                filtered.set(i, null);
        }

        //for(int i = 0; i < filtered.size(); i++) {
        //    Advert semiFiltered = filtered.get(i);
        //    if (searchQuery.getTitle().equals("")) break;
        //    else if (!semiFiltered.getTitle().contains(searchQuery.getTitle()))
        //        bools.set(i, false);
        //}

        //for(int i = 0; i < filtered.size(); i++) {
        //    Advert semiFiltered = filtered.get(i);
        //    if (searchQuery.getPlace().equals("")) break;
        //    else if (!semiFiltered.getPlace().contains(searchQuery.getPlace()))
        //        filtered.remove(semiFiltered);
        //}

        //for(int i = 0; i < filtered.size(); i++) {
        //    Advert semiFiltered = filtered.get(i);
//
        //    System.out.println(searchQuery.getCreation_date());
        //    if(searchQuery.getCreation_date() == null) {
        //        break;
        //    }
//
        //    Calendar calendar = new GregorianCalendar();
        //    Date data = semiFiltered.getCreation_date();
        //    calendar.setTime(data);
        //    int yearAdvert = calendar.get(Calendar.YEAR);
        //    int dayAdvert = calendar.get(Calendar.DAY_OF_MONTH);
        //    int monthAdvert = calendar.get(Calendar.MONTH);
//
        //    data = searchQuery.getCreation_date();
        //    calendar.setTime(data);
        //    int qyearAdvert = calendar.get(Calendar.YEAR);
        //    int qdayAdvert = calendar.get(Calendar.DAY_OF_MONTH);
        //    int qmonthAdvert = calendar.get(Calendar.MONTH);
//
        //    if(qyearAdvert != yearAdvert || qdayAdvert != dayAdvert || qmonthAdvert != monthAdvert) {
        //        filtered.remove(semiFiltered);
        //    }
        //}

        //for(int i = 0; i < filtered.size(); i++) {
        //    Advert semiFiltered = filtered.get(i);
        //    if(searchQuery.getMax_participants() == 0)
        //        break;
//
        //    if(semiFiltered.getMax_participants() != searchQuery.getMax_participants())
        //        filtered.remove(semiFiltered);
        //}

        //for(int i = 0; i < filtered.size(); i++) {
        //    Advert semiFiltered = filtered.get(i);
//
        //    if(searchQuery.getEvent_date() == null) {
        //        break;
        //    }
        //    System.out.println(semiFiltered.getTitle());
        //    Calendar calendar = new GregorianCalendar();
        //    Date data = semiFiltered.getEvent_date();
        //    calendar.setTime(data);
        //    int yearAdvert = calendar.get(Calendar.YEAR);
        //    int dayAdvert = calendar.get(Calendar.DAY_OF_MONTH);
        //    int monthAdvert = calendar.get(Calendar.MONTH);
//
        //    data = searchQuery.getEvent_date();
        //    calendar.setTime(data);
        //    int qyearAdvert = calendar.get(Calendar.YEAR);
        //    int qdayAdvert = calendar.get(Calendar.DAY_OF_MONTH);
        //    int qmonthAdvert = calendar.get(Calendar.MONTH);
//
        //    if(qyearAdvert != yearAdvert || qdayAdvert != dayAdvert || qmonthAdvert != monthAdvert){
        //        filtered.remove(semiFiltered);
        //    }
        //    else{
        //        //filtered.add(semiFiltered);
        //        System.out.println("==================");
        //        System.out.println(qyearAdvert);
        //        System.out.println(qdayAdvert);
        //        System.out.println(qmonthAdvert);
        //        System.out.println("---------");
        //        System.out.println(yearAdvert);
        //        System.out.println(dayAdvert);
        //        System.out.println(monthAdvert);
        //        System.out.println("===================");
        //    }
        //}
        //System.out.println("sddddddddddddddddddd");
        //for(Advert filt: filtered)
        //{
        //    if (filt == null) continue;
        //    System.out.println(filt.getTitle());
        //}

        return filtered;
    }

    @PostMapping(value="/removeAdvert")
    public String removeAdvert(@RequestParam("id") String advertId){
        if (!advertId.equals("none") && advertId != null && !advertId.equals("")) {
            Advert article = advertRepository.findOne(advertId);
            advertRepository.delete(article);
        }else {
            return "ERROR! ID NOT FOUND";
        }
        return "Advertisements";
    }


    @RequestMapping(value="/getAllLikesById")
    public List<AdvertLike> getAllAdverts(@RequestParam("id") String advertId)
    {
        List<AdvertLike> likes = advertLikeRepository.findAll();
        List<AdvertLike> finalLikes = new ArrayList<AdvertLike>();
        for(AdvertLike like: likes)
        {
            if (like.getAdvertId().equals(advertId))
                finalLikes.add(like);
        }
        return finalLikes;
    }

    @PostMapping(value="/getLikeById")
    public AdvertLike getLikeById(@RequestBody AdvertLike advertLike /*@RequestParam("id") String advertId, @RequestParam("uid") String userId*/)
    {
        if (!advertLike.getAdvertId().equals("none") && advertLike.getAdvertId() != null && !advertLike.getAdvertId().equals("")) {
            if (!advertLike.getUserId().equals("none") && advertLike.getUserId() != null && !advertLike.getUserId().equals("")) {
                List<AdvertLike> likes = advertLikeRepository.findAll();
                for(AdvertLike like: likes)
                {
                    if(like.getAdvertId().equals(advertLike.getAdvertId()) && like.getUserId().equals(advertLike.getUserId()))
                        return like;
                }
            }
        }
        return new AdvertLike(null, null, null, null);
    }

    @PostMapping(value="/addAdvertLike")
    public String addAdvertLike(@RequestBody AdvertLike advertLike, @RequestParam("id") String advertId, @RequestParam("uid") String userId){
        if (!advertId.equals("none") && advertId != null && !advertId.equals("")) {
            if (!userId.equals("none") && userId != null && !userId.equals("")) {
                List<AdvertLike> allLikes = advertLikeRepository.findAll();
                for(AdvertLike currentLike: allLikes)
                {
                    if(currentLike.getAdvertId().equals(advertId) && currentLike.getUserId().equals(userId)) {
                        AdvertLike like = currentLike;
                        advertLikeRepository.delete(currentLike);
                        like.setDateOfLike(new Date());
                        advertLikeRepository.save(like);
                        return "Advertisements";
                    }
                }
                advertLike.setDateOfLike(new Date());
                advertLikeRepository.save(advertLike);
                return "Advertisements";
            } else{
                return "ERROR! USER ID NOT FOUND";
            }
        }else {
            return "ERROR! ADVERT ID NOT FOUND";
        }
    }

    @PostMapping(value="/removeAdvertLike")
    public String removeAdvertLike(@RequestParam("id") String advertId, @RequestParam("uid") String userId){
        if (!advertId.equals("none") && advertId != null && !advertId.equals("")) {
            if (!userId.equals("none") && userId != null && !userId.equals("")) {
                List<AdvertLike> allLikes = advertLikeRepository.findAll();
                for(AdvertLike currentLike: allLikes)
                {
                    if(currentLike.getAdvertId().equals(advertId) && currentLike.getUserId().equals(userId)) {
                        AdvertLike like = currentLike;
                        advertLikeRepository.delete(currentLike);
                        return "Advertisements";
                    }
                }
                return "Advertisements";
            } else{
                return "ERROR! USER ID NOT FOUND";
            }
        }else {
            return "ERROR! ADVERT ID NOT FOUND";
        }
    }

    @RequestMapping(value="/getAllParticipantsById")
    public List<AdvertParticipant> getAllParticipantsById(@RequestParam("id") String advertId)
    {
        List<AdvertParticipant> participants = advertParticipantRepository.findAll();
        List<AdvertParticipant> finalParticipants = new ArrayList<AdvertParticipant>();
        for(AdvertParticipant participant: participants)
        {
            if (participant.getAdvertId().equals(advertId))
                finalParticipants.add(participant);
        }
        return finalParticipants;
    }

    @PostMapping(value="/getParticipantById")
    public AdvertParticipant getParticipantById(@RequestBody AdvertParticipant advertPart/*@RequestParam("id") String advertId, @RequestParam("uid") String userId*/)
    {
        if (!advertPart.getParticipantId().equals("none") && advertPart.getParticipantId() != null && !advertPart.getParticipantId().equals("")) {
            if (!advertPart.getAdvertId().equals("none") && advertPart.getAdvertId() != null && !advertPart.getAdvertId().equals("")) {
                List<AdvertParticipant> participants = advertParticipantRepository.findAll();
                for(AdvertParticipant participant: participants)
                {
                    if(participant.getAdvertId().equals(advertPart.getAdvertId()) && participant.getParticipantId().equals(advertPart.getParticipantId()))
                        return participant;
                }
            }
        }
        return new AdvertParticipant(null, null, null, null);
    }

    @PostMapping(value="/addParticipant")
    public String addParticipant(@RequestBody AdvertParticipant advertPart, @RequestParam("id") String advertId, @RequestParam("pid") String participantId){
        if (!advertId.equals("none") && advertId != null && !advertId.equals("")) {
            if (!participantId.equals("none") && participantId != null && !participantId.equals("")) {
                List<AdvertParticipant> allParticipants = advertParticipantRepository.findAll();
                for(AdvertParticipant currentParticipant: allParticipants)
                {
                    if(currentParticipant.getAdvertId().equals(advertId) && currentParticipant.getParticipantId().equals(participantId)) {
                        AdvertParticipant part = currentParticipant;
                        advertParticipantRepository.delete(currentParticipant);
                        part.setRegisterDate(new Date());
                        advertParticipantRepository.save(part);
                        return "Advertisements";
                    }
                }
                advertPart.setRegisterDate(new Date());
                advertParticipantRepository.save(advertPart);
                return "Advertisements";
            } else{
                return "ERROR! PARTICIPANT ID NOT FOUND";
            }
        }else {
            return "ERROR! ADVERT ID NOT FOUND";
        }
    }

    @PostMapping(value="/removeParticipant")
    public String removeParticipant(@RequestParam("id") String advertId, @RequestParam("pid") String participantId){
        if (!advertId.equals("none") && advertId != null && !advertId.equals("")) {
            if (!participantId.equals("none") && participantId != null && !participantId.equals("")) {
                List<AdvertParticipant> allParticipants = advertParticipantRepository.findAll();
                for(AdvertParticipant currentParticipant: allParticipants)
                {
                    if(currentParticipant.getAdvertId().equals(advertId) && currentParticipant.getParticipantId().equals(participantId)) {
                        AdvertParticipant part = currentParticipant;
                        advertParticipantRepository.delete(currentParticipant);
                        return "Advertisements";
                    }
                }
            } else{
                return "ERROR! PARTICIPANT ID NOT FOUND";
            }
        }else {
            return "ERROR! ADVERT ID NOT FOUND";
        }
        return "Advertisements";
    }

}
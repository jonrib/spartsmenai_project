package com.spartsmenai.ktu.MVC;

import com.mongodb.util.JSON;
import com.spartsmenai.ktu.Entities.Article;
import com.spartsmenai.ktu.Entities.Category;
import com.spartsmenai.ktu.Entities.Game;
import com.spartsmenai.ktu.MVC.GamesController.SortByDateAsc;
import com.spartsmenai.ktu.Repositories.ArticleRepository;
import com.spartsmenai.ktu.Repositories.CategoryRepository;
import com.spartsmenai.ktu.Services.ArticleService;
import com.spartsmenai.ktu.Services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

@RestController
@RequestMapping("/articles")
public class ArticleController {

    @Autowired
    private ArticleService articleService;
    
    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ArticleRepository articleRepository;
    
    @Autowired
    private CategoryRepository categoryRepository;
    
    private List<Article> allArticles = new ArrayList<Article>();
    
    @RequestMapping(method = RequestMethod.GET)
    public List<Article> getAllArticles(){
        return articleService.getAllArticles();
    }
    
    private List<Category> allCategories = new ArrayList<Category>();
    
    @RequestMapping(method = RequestMethod.GET, value="/categories")
    public List<Category> getAllCategories(){
        return categoryService.getAllCategories();
    }
    
    @PostMapping(value = "/findByName")
    public List<Article> getAllArticlesByName(@RequestParam("query") String query){
        List<Article> all =  articleService.getAllArticles();
        List<Article> filtered = new ArrayList<Article>();
        for (Article art : all) {
			if (art.getName().toLowerCase().contains(query.toLowerCase()))
				filtered.add(art);
			
		}
        return filtered;
    }
    
    @PostMapping(value = "/findByTagsAndCategory")
    public List<Article> getAllArticlesByTagsAndCategory(@RequestParam(required=false, name="tagsQuery") String tagsQuery, @RequestParam(required=false, name="categoryQuery") String categoryQuery){
    	System.out.println("Tags " + tagsQuery + " Category " + categoryQuery);
        List<Article> all =  articleService.getAllArticles();
        List<Article> filtered = new ArrayList<Article>();
        for (Article art : all) {
			
        	//if (art.getCategory().get(0).getName().toLowerCase().trim() != ""?art.getCategory().get(0).getName().toLowerCase().trim().equals(categoryQuery.toLowerCase()):art.getCategory().get(0).getName().toLowerCase().trim().contains(categoryQuery.toLowerCase()))
        	if (categoryQuery.toLowerCase().trim() != "" ? art.getCategory().get(0).getName().toLowerCase().trim().equals(categoryQuery.toLowerCase().trim()):art.getCategory().get(0).getName().toLowerCase().trim().contains(categoryQuery.toLowerCase().trim()))
        	{
				String[] tagsQueryArray = tagsQuery.toLowerCase().split(";");
				String[] articleTagsArray = art.getTags().toLowerCase().split(";");
				if (tagsQueryArray.length == 0 || articleTagsArray.length == 0)
				{
					filtered.add(art);
					continue;
				}
				for (int i = 0; i < articleTagsArray.length; i++)
				{
					boolean hasFound = false;
					for (int j = 0; j < tagsQueryArray.length; j++)
					{
						if (tagsQueryArray[j].trim() != ""?articleTagsArray[i].equals(tagsQueryArray[j]):articleTagsArray[i].contains(tagsQueryArray[j]))
						{
							filtered.add(art);
							hasFound = true;
							break;
						}
					}
					if (hasFound)
						break;
				}
			}
		}
        return filtered;
    }

    /*@RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public  Article getArticleById(@PathVariable("id") int id){
        return articleService.getArticleById(id);
    }*/
    
    @GetMapping(value="/findById")
    public Article findArticle(@RequestParam("id") String articleId){
    	//System.out.println(JSON.serialize(articleRepository.findOne(articleId)));
    	//return JSON.serialize(articleRepository.findOne(articleId));
    	return articleRepository.findOne(articleId);
    }
    
    
    @PostMapping(value="/save")
	public String editArticle(@RequestBody Article article, @RequestParam("id") String articleId, @RequestParam("category") String category){
    	Category cat = null;
    	allCategories = getAllCategories();
        List<Category> filtered = new ArrayList<Category>();
        for (Category categ : allCategories) {
			if (categ.getName().equals(category)) {
				filtered.add(categ);
				cat = categ;
				cat.setName(cat.getName().toLowerCase());
			}
				
		}
        if (filtered.isEmpty()) {
        	cat = new Category(category.toLowerCase());
        	categoryRepository.save(cat);
        }
        	
		article.setCategoryName(cat);
    	if (!articleId.equals("none") && articleId != null && !articleId.equals("")) {
			
			//System.out.println("Article " + (article == null));
			
			Article oldEntity = articleRepository.findOne(articleId);
			//System.out.println("Oldarticle " + (oldEntity == null));
			Date now = new Date();
			article.setUpdate_date(now);
			//article.setUpdate_date(ZonedDateTime.now().format(DateTimeFormatter.RFC_1123_DATE_TIME));
			article.setId(oldEntity.getId());
			article.setCreation_date(oldEntity.getCreation_date());
			System.out.println("Edit existing");
			articleRepository.save(article);
		}else {
			System.out.println("Create new");
			//allArticles.add(article);
			Date now = new Date();
			article.setCreation_date(now);
			article.setUpdate_date(now);
			articleRepository.save(article);
		}
		
		return "Post Successfully!";
	}
    
    class SortByDateAsc implements Comparator<Article>
    {
        // Used for sorting in ascending order of
        public int compare(Article a, Article b)
        {
            return -1*a.getUpdate_date().compareTo(b.getUpdate_date());
        }
    }
    
    @RequestMapping(value="/getArticlesSortedByDate")
    public List<Article> getArticlesSortedByDate() {
        List<Article> articles = articleRepository.findAll();

        Collections.sort(articles, new SortByDateAsc());
        return articles;
    }

    
    @PostMapping(value="/removeArticle")
	public String removeArticle(@RequestParam("id") String articleId){
		if (!articleId.equals("none") && articleId != null && !articleId.equals("")) {
			Article article = articleRepository.findOne(articleId);
			articleRepository.delete(article);
		}else {
			//allArticles.add(article);
			return "ERROR! ID NOT FOUND";
		}
		
		return "News";
	}
}

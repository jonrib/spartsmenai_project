/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spartsmenai.ktu.MVC;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.spartsmenai.ktu.Entities.ActiveUser;
import com.spartsmenai.ktu.Entities.Article;
import com.spartsmenai.ktu.Entities.Comment;
import com.spartsmenai.ktu.Entities.Discussion;
import com.spartsmenai.ktu.Entities.User;
import com.spartsmenai.ktu.Repositories.ActiveUserRepository;
import com.spartsmenai.ktu.Repositories.ArticleRepository;
import com.spartsmenai.ktu.Repositories.CommentRepository;
import com.spartsmenai.ktu.Repositories.DiscussionRepository;
import com.spartsmenai.ktu.Repositories.UserRepository;
import com.spartsmenai.ktu.Services.ArticleService;
import com.spartsmenai.ktu.Services.CommentService;
import com.spartsmenai.ktu.Services.DiscussionService;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@RestController
@RequestMapping("/forums")
public class DicussionsController {
	@Autowired
    private DiscussionService discussionService;

    @Autowired
    private DiscussionRepository discussionRepository;
    
    @Autowired
    private CommentService commentService;

    @Autowired
    private CommentRepository commentRepository;
    
    @Autowired
    private ActiveUserRepository activeUserRepository;
    
    @Autowired
    private UserRepository userRepository;
    
    private List<Discussion> allDiscussions = new ArrayList<Discussion>();
    
    @RequestMapping(method = RequestMethod.GET)
    public List<Discussion> getAllDiscussions(){
        return discussionService.getAllDiscussions();
    }
	
    @PostMapping(value = "/findByName")
    public List<Discussion> getAllDiscussionsByName(@RequestParam("query") String query){
        List<Discussion> all =  discussionService.getAllDiscussions();
        List<Discussion> filtered = new ArrayList<Discussion>();
        for (Discussion art : all) {
			if (art.getTopic().contains(query))
				filtered.add(art);
			
		}
        return filtered;
    }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
	
	
	@RequestMapping(value = "/discussion")
	public String index() {
		return "Discussion";
	}
	
	@GetMapping(value="/findById")
    public Discussion findDiscussion(@RequestParam("id") String discussionId){
    	return discussionRepository.findOne(discussionId);
    }
    
    
    @PostMapping(value="/save")
	public String editDiscussion(@RequestBody Discussion discussion, @RequestParam("id") String discussionId, HttpServletRequest request){
		if (!discussionId.equals("none") && discussionId != null && !discussionId.equals("")) {
			Discussion oldEntity = discussionRepository.findOne(discussionId);
			discussionRepository.delete(oldEntity);
			discussion.setCreation_date(oldEntity.getCreation_date());
			discussion.setUpdate_date(new Date());
			discussion.setComments((ArrayList<Comment>) oldEntity.getComments());
			//set user
			discussion.setUser(oldEntity.getUser());
			//-----
			discussionRepository.save(discussion);
		}else {
			discussion.setCreation_date(new Date());
			discussion.setUpdate_date(new Date());
			
			//Getting active user
			ActiveUser userToSet = new ActiveUser("Anonimas", "", 0, "", "", "", request.getRemoteAddr());
			List<ActiveUser> allUsers = activeUserRepository.findAll();
			for (ActiveUser user : allUsers) {
				if (user.getIp().equals(request.getRemoteAddr())) {
					userToSet = user;
				}
			}
			
			User user = new User("Anonimas", "", 0, "", "");
			List<User> all = userRepository.findAll();
			for (User usr : all) {
				if (usr.getUsername().equals(userToSet.getUsername())) {
					user = usr;
				}
			}
			//----------------
			
			//set user
			discussion.addUser(user);
			//-------
			discussionRepository.save(discussion);
		}
		return "Post Successfully!";
	}
    
    @PostMapping(value="/saveComment")
	public Comment saveComment(@RequestBody Comment comment, @RequestParam("id") String discussionId, HttpServletRequest request){
		if (!discussionId.equals("none") && discussionId != null && !discussionId.equals("")) {
			Discussion disc = discussionRepository.findOne(discussionId);
			disc.addComment(comment);
			comment.setCommentDate(new Date());
			comment.setEditDate(new Date());
			//NEEDS TO SET USER
			
			//Getting active user
			ActiveUser userToSet = new ActiveUser("Anonimas", "", 0, "", "","", request.getRemoteAddr());
			List<ActiveUser> allUsers = activeUserRepository.findAll();
			for (ActiveUser user : allUsers) {
				if (user.getIp().equals(request.getRemoteAddr())) {
					userToSet = user;
				}
			}
			
			User user = new User("Anonimas", "", 0, "", "");
			List<User> all = userRepository.findAll();
			for (User usr : all) {
				if (usr.getUsername().equals(userToSet.getUsername())) {
					user = usr;
				}
			}
			comment.setUser(user);
			//----------------
			
			
			commentRepository.save(comment);
			discussionRepository.save(disc);
			return comment;
		}else {
			return null;
		}
	}
    
    @PostMapping(value="/editComment")
	public Comment editComment(@RequestBody Comment comment, @RequestParam("id") String discussionId, @RequestParam("commid") String commentId){
		if (!discussionId.equals("none") && discussionId != null && !discussionId.equals("")) {
			Discussion disc = discussionRepository.findOne(discussionId);
			Comment oldComment = commentRepository.findOne(commentId);
			
			oldComment.setEditDate(new Date());
			oldComment.setCommentText(comment.getCommentText());
			
			commentRepository.save(oldComment);
			disc.removeComment(oldComment.getId());
			disc.addComment(oldComment);
			
			discussionRepository.save(disc);
			return comment;
		}else {
			return null;
		}
	}
    
    @PostMapping(value="/removeDiscussion")
   	public String removeArticle(@RequestParam("id") String articleId){
   		if (!articleId.equals("none") && articleId != null && !articleId.equals("")) {
   			Discussion article = discussionRepository.findOne(articleId);
   			discussionRepository.delete(article);
   		}else {
   			//allArticles.add(article);
   			return "ERROR! ID NOT FOUND";
   		}
   		
   		return "Success";
   	}
    
    @PostMapping(value="/removeComment")
   	public String removeComment(@RequestParam("id") String articleId, @RequestParam("disc") String discId){
   		if (!articleId.equals("none") && articleId != null && !articleId.equals("")) {
   			Comment article = commentRepository.findOne(articleId);
   			Discussion disc = discussionRepository.findOne(discId);
   			disc.removeComment(articleId);
   			discussionRepository.save(disc);
   			commentRepository.delete(article);
   		}else {
   			//allArticles.add(article);
   			return "ERROR! ID NOT FOUND";
   		}
   		
   		return "Success";
   	}
    
    @GetMapping(value="/getSortedCommentsByCreated")
    public List<Comment> getSortedCommentsByCreated(@RequestParam("id") String articleId){
    	Discussion disc = discussionRepository.findOne(articleId);
    	List<Comment> comments = disc.getComments();
    	Collections.sort(comments, new SortByCreated());
    	
    	return comments;
    }
    
    @GetMapping(value="/getSortedCommentsByEdited")
    public List<Comment> getSortedCommentsByEdited(@RequestParam("id") String articleId){
    	Discussion disc = discussionRepository.findOne(articleId);
    	List<Comment> comments = disc.getComments();
    	Collections.sort(comments, new SortByCreated());
    	
    	return comments;
    }
    
    class SortByCreated implements Comparator<Comment> 
    { 
        // Used for sorting in ascending order of 
        // roll number 
        public int compare(Comment a, Comment b) 
        { 
            return a.getCommentDate().compareTo(b.getCommentDate()); 
        } 
    } 
    
    class SortByEdited implements Comparator<Comment> 
    { 
        // Used for sorting in ascending order of 
        // roll number 
        public int compare(Comment a, Comment b) 
        { 
            return a.getEditDate().compareTo(b.getEditDate()); 
        } 
    } 

}
// end::code[]
/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spartsmenai.ktu.MVC;

import com.spartsmenai.ktu.Entities.Advert;
import com.spartsmenai.ktu.Entities.Article;
import com.spartsmenai.ktu.Entities.Category;
import com.spartsmenai.ktu.Entities.Game;
import com.spartsmenai.ktu.Entities.User;
import com.spartsmenai.ktu.Repositories.ArticleRepository;
import com.spartsmenai.ktu.Repositories.CategoryRepository;
import com.spartsmenai.ktu.Repositories.GameRepository;
import com.spartsmenai.ktu.Repositories.UserRepository;
import com.spartsmenai.ktu.Services.ArticleService;
import com.spartsmenai.ktu.Services.CategoryService;
import com.spartsmenai.ktu.Services.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.spartsmenai.ktu.Services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@RestController
@RequestMapping("/gamess")
public class GamesController {
    @Autowired
    private GameService gameService;

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private CategoryService categoryService;

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    private List<Game> allGames = new ArrayList<Game>();

    @RequestMapping(method = RequestMethod.GET)
    public List<Game> getAllArticles(){
        return gameService.getAllGames();
    }

    private List<Category> allCategories = new ArrayList<Category>();

    @RequestMapping(method = RequestMethod.GET, value="/categories")
    public List<Category> getAllCategories(){
        return categoryService.getAllCategories();
    }

	@PostMapping(value="/save")
	public String editGame(@RequestBody Game game, @RequestParam("id") String gameId, @RequestParam("category") String category){
		Category cat = null;
		allCategories = getAllCategories();
		List<Category> filtered = new ArrayList<Category>();
		for (Category categ : allCategories) {
			if (categ.getName().equals(category)) {
				filtered.add(categ);
				cat = categ;
				cat.setName(cat.getName().toLowerCase());
			}

		}
		if (filtered.isEmpty()) {
			cat = new Category(category.toLowerCase());
			categoryRepository.save(cat);
		}

		game.setCategoryName(cat);
		if (!gameId.equals("none") && gameId != null && !gameId.equals("")) {

			//System.out.println("Article " + (article == null));

			Game oldEntity = gameRepository.findOne(gameId);
			//System.out.println("Oldarticle " + (oldEntity == null));

			game.setId(oldEntity.getId());
			game.setResult_a(oldEntity.getResult_a());
            game.setResult_b(oldEntity.getResult_b());
			gameRepository.save(game);
		}else {
			//allArticles.add(article);
			gameRepository.save(game);
		}

		return "Post Successfully!";
	}

	@PostMapping(value="/saveResult")
	public String editGameResult(@RequestBody Game game, @RequestParam("id") String gameId){
		
		if (!gameId.equals("none") && gameId != null && !gameId.equals("")) {

			//System.out.println("Article " + (article == null));

			Game oldEntity = gameRepository.findOne(gameId);
			System.out.println("Oldarticle " + (oldEntity == null));

			oldEntity.setResult_a(game.getResult_a());
			oldEntity.setResult_b(game.getResult_b());
			gameRepository.save(oldEntity);


		}else {
			//allArticles.add(article);
			//gameRepository.save(game);
		}

		return "Post Successfully!";
	}

	@RequestMapping(value="/findById")
    public Game findGame(@RequestParam("id") String gameId) {
        return gameRepository.findOne(gameId);
	}
	
	@PostMapping(value="/removeGame")
    public String removeGame(@RequestParam("id") String gameId){
        if (!gameId.equals("none") && gameId != null && !gameId.equals("")) {
            Game game = gameRepository.findOne(gameId);
            gameRepository.delete(game);
        }else {
            return "ERROR! ID NOT FOUND";
        }
        return "Games";
	}

    class SortByTitleAsc implements Comparator<Game>
    {
        // Used for sorting in ascending order of
        public int compare(Game a, Game b)
        {
            return -1*a.getName().compareTo(b.getName());
        }
    }

	@RequestMapping(value="/getGamesSortedByTitleAsc")
    public List<Game> getGamesSortedByTitleAsc() {
        List<Game> games = gameRepository.findAll();
        Collections.sort(games, new SortByTitleAsc());
        return games;
    }

    class SortByDateAsc implements Comparator<Game>
    {
        // Used for sorting in ascending order of
        public int compare(Game a, Game b)
        {
            return a.getEvent_date().compareTo(b.getEvent_date());
        }
    }

    @RequestMapping(value="/getGamesSortedByEventDateAsc")
    public List<Game> getGamesSortedByEventDateAsc() {
        List<Game> games = gameRepository.findAll();

        Collections.sort(games, new SortByDateAsc());
        return games;
    }


    @PostMapping(value = "/findByCategory")
    public List<Game> getAllGamesByCategory(@RequestParam("categoryQuery") String categoryQuery){
        List<Game> all =  gameRepository.findAll();
        List<Game> filtered = new ArrayList<Game>();
        for (Game gam : all) {
			
        	if (categoryQuery.toLowerCase().trim() != "" ? gam.getCategory().get(0).getName().toLowerCase().trim().equals(categoryQuery.toLowerCase().trim()):gam.getCategory().get(0).getName().toLowerCase().trim().contains(categoryQuery.toLowerCase().trim()))
        	{
					filtered.add(gam);
					continue;
			}
		}
        return filtered;
    }

    
    @GetMapping(value = "/addtogame")
    public String Addusertogame(@RequestParam("nameid") String userid, @RequestParam("gameid") String gameid, @RequestParam("_") String useless){
    	
    	
        List<Game> all =  gameRepository.findAll();
        //List<Game> filtered = new ArrayList<Game>();
        for (Game gam : all) {
        	
        	if (gam.getId().equals(gameid))
        	{
        		List<User> all1 = userRepository.findAll();
        		for (User us : all1) {
        			
        			if(us.getId().equals(userid)) {
        				//System.out.println(us.getUsername());
        				
        				gam.setActiveUser(us);
        				gameRepository.save(gam);
        			}
        		}
			}
		}
        return "News";
    }
}
// end::code[]
/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spartsmenai.ktu.MVC;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Controller
public class HomeController {

	@RequestMapping(value = "/news")
	public String news() {
		return "News";
	}
	
	@RequestMapping(value = "/addMembertoGame")
	public String addMembertoGame() {
		return "AddMembertoGame";
	}
	@RequestMapping(value = "/forum")
	public String forum() {
		return "Forum";
	}
	@RequestMapping(value = "/games")
	public String games() {
		return "Games";
	}
	@RequestMapping(value = "/game/*")
	public String game() {
		return "Game";
	}
	@RequestMapping(value = "/removeGame/*")
	public String removeGame() {
		return "RemoveGame";
	}
	@RequestMapping(value = "/editGame")
	public String editGame() {
		return "EditGame";
	}
	@RequestMapping(value = "/editGameResult")
	public String editGameResult() {
		return "EditGameResult";
	}
	@RequestMapping(value = "/advertisements")
	public String advertisements() {
		return "Advertisements";
	}
	@RequestMapping(value = "/")
	public String index() {
		return "News";
	}
	@RequestMapping(value = "/registration")
	public String registration() {
		return "Registration";
	}
	@RequestMapping(value = "/login")
	public String login() {
		return "Login";
	}
	
	
	
	@RequestMapping(value = "/listofusers")
	public String users() {
		return "ListOfUsers";
	}
	
	
	
	@RequestMapping(value = "/profile")
	public String profile() {
		return "Profile";
	}
	@RequestMapping(value = "/detailedprofile")
	public String detailedprofile() {
		return "DetailedProfile";
	}
	
	@RequestMapping(value = "/news/*")
	public String ANew() {
		return "ANew";
	}
	@RequestMapping(value = "/editANew/*")
	public String editANew() {
		return "EditANew";
	}
	@RequestMapping(value = "/removeANew/*")
	public String removeNews() {
		return "RemoveANew";
	}
	@RequestMapping(value = "/editArticle")
	public String EditArticle() {
		return "EditArticle";
	}
	@RequestMapping(value = "/article")
	public String Article() {
		return "Article";
	}
	@RequestMapping(value = "/editDiscussion")
	public String EditDiscussion() {
		return "EditDiscussion";
	}

	@RequestMapping(value = "/editAdvert")
	public String EditAdvert() {return "Advert-edit";}
	@RequestMapping(value = "/openAdvert")
	public String OpenAdvert() {return "Advert";}

	@RequestMapping(value = "/openGame")
	public String OpenGame() {return "Game";}

	@RequestMapping(value = "/pendingMembers")
	public String pendingMembers() {
		return "PendingMembers";
	}
	

}
// end::code[]
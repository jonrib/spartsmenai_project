package com.spartsmenai.ktu.MVC;

import com.mongodb.util.JSON;
import com.spartsmenai.ktu.Entities.Article;
import com.spartsmenai.ktu.Entities.User;
import com.spartsmenai.ktu.Repositories.ArticleRepository;
import com.spartsmenai.ktu.Repositories.UserRepository;
import com.spartsmenai.ktu.Services.ArticleService;
import com.spartsmenai.ktu.Services.UserService; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

	  @Autowired
	    private UserService userService;

	    @Autowired
	    private UserRepository userRepository;
	    
	    private List<User> allUsers = new ArrayList<User>();
	    
	    @RequestMapping(method = RequestMethod.GET)
	    public List<User> getAllUsers(){
	        return userService.getAllUsers();
	    }
	    
	    
	    
	    
	    @PostMapping(value="/getByUsername")
		public String getByUsername(@RequestParam("username") String userId, @RequestParam("pass") String pass){
			List<User> allUsers = userRepository.findAll();
			for (User user : allUsers) {
				if (user.getUsername().equals(userId) && user.getPassword().equals(pass)) {
					return "success";
				}
				
			}
			return "failed";
		}
	    
	    @PostMapping(value = "/findByName")
	    public List<User> getAllusersByName(@RequestParam("query") String query){
	        List<User> all =  userService.getAllUsers();
	        List<User> filtered = new ArrayList<User>();
	        for (User us : all) {
				if (us.getUsername().contains(query))
					filtered.add(us);
				
			}
	        return filtered;
	    }

	    
	    @GetMapping(value="/findById")
	    public User findUser(@RequestParam("id") String userId){
	    	//System.out.println(JSON.serialize(articleRepository.findOne(articleId)));
	    	//return JSON.serialize(articleRepository.findOne(articleId));
	    	return userRepository.findOne(userId);
	    }
	    
	    
	    @PostMapping(value="/save")
		public String editUserProfile(@RequestBody User user, @RequestParam("id") String userId){
			if (!userId.equals("none") && userId != null && !userId.equals("")) {
				
				//System.out.println("Article " + (article == null));
				
				User oldEntity = userRepository.findOne(userId);
				
				//System.out.println("Oldarticle " + (oldEntity == null));
				user.setId(oldEntity.getId());
				userRepository.save(user);
			}else {
				//allArticles.add(article);
				userRepository.save(user);
			}
						
			return "Post Successfully!";
		}
	    
	    
	    @PostMapping(value="/removeUser")
		public String removeUser(@RequestParam("id") String userId){
			if (!userId.equals("none") && userId != null && !userId.equals("")) {
				User user = userRepository.findOne(userId);
				userRepository.delete(user);
			}else {
				//allArticles.add(article);
				return "ERROR! ID NOT FOUND";
			}
			
			return "News";
		}
	
	
}

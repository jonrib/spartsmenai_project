package com.spartsmenai.ktu.Repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.spartsmenai.ktu.Entities.ActiveUser;

@Repository
public interface ActiveUserRepository extends MongoRepository<ActiveUser,String> {

}

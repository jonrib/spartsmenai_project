package com.spartsmenai.ktu.Repositories;

import com.spartsmenai.ktu.Entities.AdvertLike;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdvertLikeRepository extends MongoRepository<AdvertLike,String> {
}

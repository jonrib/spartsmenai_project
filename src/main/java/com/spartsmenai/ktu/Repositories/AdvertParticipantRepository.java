package com.spartsmenai.ktu.Repositories;

import com.spartsmenai.ktu.Entities.AdvertParticipant;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdvertParticipantRepository extends MongoRepository<AdvertParticipant,String> {
}

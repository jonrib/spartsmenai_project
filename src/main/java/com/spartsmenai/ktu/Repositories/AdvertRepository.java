package com.spartsmenai.ktu.Repositories;

import com.spartsmenai.ktu.Entities.Advert;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdvertRepository extends MongoRepository<Advert,String> {
}

package com.spartsmenai.ktu.Repositories;

import com.spartsmenai.ktu.Entities.Category;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends MongoRepository<Category,String> {

}

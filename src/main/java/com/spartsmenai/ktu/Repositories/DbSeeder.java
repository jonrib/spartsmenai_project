package com.spartsmenai.ktu.Repositories;

import com.spartsmenai.ktu.Entities.Article;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Component
public class DbSeeder implements CommandLineRunner {
    private ArticleRepository articleRepository;

    public DbSeeder(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @Override
    public void run(String... string) throws Exception {
        //Article vienas = new Article("Sveiki","Zyme",10.0,new Date(), new Date(), "pvksl","saltinis","Achujebytelnas straipsnelis");
        //Article du = new Article("Sveiki1","Zyme1",10.1,new Date(), new Date(), "pvksl","saltinis","Achujebytelnas straipsnelis");
        //Article trys = new Article("Sveiki2","Zyme2",10.2,new Date(), new Date(), "pvksl","saltinis","Achujebytelnas straipsnelis");

        // drop all hotels

        //this.articleRepository.deleteAll();

        // add

        //List<Article> articles = Arrays.asList(vienas, du, trys);
        //this.articleRepository.save(articles);
    }
}

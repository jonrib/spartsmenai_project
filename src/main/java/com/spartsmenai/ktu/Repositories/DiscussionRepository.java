package com.spartsmenai.ktu.Repositories;

import com.spartsmenai.ktu.Entities.Discussion;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiscussionRepository extends MongoRepository<Discussion,String> {

}

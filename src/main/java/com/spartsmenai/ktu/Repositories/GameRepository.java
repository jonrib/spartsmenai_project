package com.spartsmenai.ktu.Repositories;
import com.spartsmenai.ktu.Entities.Game;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepository extends MongoRepository<Game,String> {

}

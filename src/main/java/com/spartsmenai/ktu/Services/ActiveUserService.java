package com.spartsmenai.ktu.Services;

import com.spartsmenai.ktu.Entities.ActiveUser;
import com.spartsmenai.ktu.Repositories.ActiveUserRepository;
//import com.spartsmenai.ktu.fakerino.fake;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class ActiveUserService {

	
	 @Autowired
	    //private fake Fake;

	    private ActiveUserRepository activeuserRepository;

	    public ActiveUserService(ActiveUserRepository activeuserRepository) {
	        this.activeuserRepository = activeuserRepository;
	    }



	    public List<ActiveUser> getAllUsers(){
	        return activeuserRepository.findAll();
	    }
	    
	    /*public Article getArticleById(int id){
	        return articleRepository.getArticleById(id);
	    }*/
	    
}

package com.spartsmenai.ktu.Services;

import com.spartsmenai.ktu.Entities.Advert;
import com.spartsmenai.ktu.Entities.AdvertCategory;
import com.spartsmenai.ktu.Repositories.AdvertCategoryRepository;
import com.spartsmenai.ktu.Repositories.AdvertRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdvertService {
    @Autowired
    //private fake Fake;

    private AdvertRepository advertRepository;
    private AdvertCategoryRepository advertCategoryRepository;

    public AdvertService(AdvertRepository advertRepository) {
        this.advertRepository = advertRepository;
    }

    public List<Advert> getAllAdverts(){ return advertRepository.findAll(); }



}

package com.spartsmenai.ktu.Services;

import com.spartsmenai.ktu.Entities.Article;
import com.spartsmenai.ktu.Repositories.ArticleRepository;
//import com.spartsmenai.ktu.fakerino.fake;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class ArticleService {
    @Autowired
    //private fake Fake;

    private ArticleRepository articleRepository;

    public ArticleService(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }



    public List<Article> getAllArticles(){
        return articleRepository.findAll();
    }
    /*public Article getArticleById(int id){
        return articleRepository.getArticleById(id);
    }*/
}

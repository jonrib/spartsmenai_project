package com.spartsmenai.ktu.Services;

//import com.spartsmenai.ktu.fakerino.fake;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spartsmenai.ktu.Entities.Category;
import com.spartsmenai.ktu.Repositories.CategoryRepository;

import java.util.List;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }



    public List<Category> getAllCategories(){
        return categoryRepository.findAll();
    }
}

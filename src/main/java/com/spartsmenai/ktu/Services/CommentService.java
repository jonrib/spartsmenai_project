package com.spartsmenai.ktu.Services;

//import com.spartsmenai.ktu.fakerino.fake;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spartsmenai.ktu.Entities.Comment;
import com.spartsmenai.ktu.Repositories.CommentRepository;

import java.util.List;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;

    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }



    public List<Comment> getAllComments(){
        return commentRepository.findAll();
    }
}

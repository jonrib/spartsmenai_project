package com.spartsmenai.ktu.Services;

import com.spartsmenai.ktu.Entities.Article;
import com.spartsmenai.ktu.Entities.Discussion;
import com.spartsmenai.ktu.Repositories.ArticleRepository;
import com.spartsmenai.ktu.Repositories.DiscussionRepository;

//import com.spartsmenai.ktu.fakerino.fake;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class DiscussionService {
    @Autowired
    //private fake Fake;

    private DiscussionRepository discussionRepository;

    public DiscussionService(DiscussionRepository discussionRepository) {
        this.discussionRepository = discussionRepository;
    }



    public List<Discussion> getAllDiscussions(){
        return discussionRepository.findAll();
    }
    /*public Article getArticleById(int id){
        return articleRepository.getArticleById(id);
    }*/
}

package com.spartsmenai.ktu.Services;

import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

//Example below
/*
import com.ClienteleSoft.CMS.Entity.Employee;
import com.ClienteleSoft.CMS.Entity.User;
import com.ClienteleSoft.CMS.Repositories.EmployeeRepository;
import com.ClienteleSoft.CMS.Repositories.RoleRepository;
import com.ClienteleSoft.CMS.Repositories.UserRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public List<Employee> findByUsername(String username) {
        return employeeRepository.findByUsername(username);
    }
}
*/
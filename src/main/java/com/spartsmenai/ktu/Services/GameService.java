package com.spartsmenai.ktu.Services;

import com.spartsmenai.ktu.Entities.Discussion;
import com.spartsmenai.ktu.Entities.Game;
import com.spartsmenai.ktu.Repositories.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//import com.spartsmenai.ktu.fakerino.fake;

@Service
public class GameService {
    @Autowired
    //private fake Fake;

    private GameRepository gameRepository;

    public GameService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }



    public List<Game> getAllGames(){
        return gameRepository.findAll();
    }
    /*public Article getArticleById(int id){
        return articleRepository.getArticleById(id);
    }*/
}

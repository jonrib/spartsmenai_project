package com.spartsmenai.ktu.Services;

import com.spartsmenai.ktu.Entities.Article;
import com.spartsmenai.ktu.Entities.User;
import com.spartsmenai.ktu.Repositories.UserRepository;
//import com.spartsmenai.ktu.fakerino.fake;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class UserService {

	
	 @Autowired
	    //private fake Fake;

	    private UserRepository userRepository;

	    public UserService(UserRepository userRepository) {
	        this.userRepository = userRepository;
	    }



	    public List<User> getAllUsers(){
	        return userRepository.findAll();
	    }
	    
	    /*public Article getArticleById(int id){
	        return articleRepository.getArticleById(id);
	    }*/
	    
}

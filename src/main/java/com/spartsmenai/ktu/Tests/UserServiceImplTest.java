package com.spartsmenai.ktu.Tests;

import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

//Example test
/*
import com.ClienteleSoft.CMS.Entity.Role;
import com.ClienteleSoft.CMS.Entity.User;
import com.ClienteleSoft.CMS.Repositories.UserRepository;
import com.ClienteleSoft.CMS.Services.UserService;
import com.ClienteleSoft.CMS.Services.UserServiceImpl;
import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
public class UserServiceImplTest {
 
    @TestConfiguration
    @Configuration
    @ComponentScan("com.ClienteleSoft.CMS")
    @EnableMongoRepositories(basePackages = "com.ClienteleSoft.CMS.Repositories")
    @SpringBootApplication
    static class EmployeeServiceImplTestContextConfiguration {
  
        @Bean
        public UserServiceImpl userServiceImpl() {
            return new UserServiceImpl();
        }
    }
 
    @Autowired
    private UserService userService;
 
    @MockBean
    private UserRepository userRepository;
    
    @Before
    public void setUp() {
        User alex = new User("123456", "alex", "test", "test", null);
     
        Mockito.when(userRepository.findByUsername(alex.getUsername()))
          .thenReturn(alex);
    }
 
    // write test cases here
    @Test
    public void whenValidUserName_thenUserShouldBeFound() {
        String username = "alex";
        User found = userService.findByUsername(username);
      
         assertThat(found.getUsername())
          .isEqualTo(username);
     }
    
}
*/
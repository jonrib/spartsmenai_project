/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spartsmenai.ktu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(scanBasePackages={
		"com.spartsmenai.ktu", 
		"com.spartsmenai.ktu.Services",
		"com.spartsmenai.ktu.Configuration",
		"com.spartsmenai.ktu.Entities",
		"com.spartsmenai.ktu.MVC",
		"com.spartsmenai.ktu.Repositories",
		"com.spartsmenai.ktu.Validators",
		"com.spartsmenai.ktu.fakerino"})
@ComponentScan({
	"com.spartsmenai.ktu", 
	"com.spartsmenai.ktu.Services",
	"com.spartsmenai.ktu.Configuration",
	"com.spartsmenai.ktu.Entities",
	"com.spartsmenai.ktu.MVC",
	"com.spartsmenai.ktu.Repositories",
	"com.spartsmenai.ktu.Validators",
		"com.spartsmenai.ktu.fakerino"})
public class WebApplication extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(WebApplication.class);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(WebApplication.class, args);
    }
}
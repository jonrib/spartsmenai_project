$(document).ready(function () {
    console.log("here");
    var url = new URL(window.location.href);
    var id = url.searchParams.get("id");
    $.ajax({
        type: "GET",
        contentType: "application/json",
        accept: 'text/plain',
        url: "/activeuser/findactiveuser",
        dataType: 'text',
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            
            $("a[href*='news']").css("display", "");
        	$("a[href*='games']").css("display", "");
        	$("a[href*='advertisements']").css("display", "");
        	$("a[href*='forum']").css("display", "");
        	$(".btn-primary").css("display","");
        	
        	if(JSON.parse(data).user_level < 4){
        		$("a[href*='registration']").css("display", "");
        		$("a[href*='login']").css("display", "");
        		$("a[onclick*='javascript: $.ajax({type: ']").css("display", "none");
        	}
        
        if(JSON.parse(data).user_level >= 4){
        	
        	$("a[href*='listofusers']").css("display", "");
       	 $("a[href*='profile']").css("display", "");
       	 $("a[onclick*='javascript: $.ajax({type: ']").css("display", "");
        }
            
        },
        error: function (e) {
            console.log(e);
        }
    });
    if (id != "" && id != null && typeof id != "undefined"){
        getSingleAdvert(id);
    }
});

function getDateFormat(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    var date = new Date();
    date.toLocaleDateString();

    return [year, month, day].join('-');
};

function getSingleAdvert(id){
    $.ajax({
        type : "GET",
        contentType : "application/json",
        accept: 'text/plain',
        url : "adverts/findById?id="+id,
        dataType: 'text',
        async: false,
        success : function(data) {
            $("#title").val(JSON.parse(data).title);
            $("#description").val(JSON.parse(data).description);
            $("#place").val(JSON.parse(data).place);
            $("#image").val(JSON.parse(data).imageLink);
            $("#max_participants").val(JSON.parse(data).max_participants);
            $("#event_date").val(getDateFormat(JSON.parse(data).event_date));
        },
        error : function(e) {
            alert("Error!")
            console.log("ERROR: ", e);
        }
    });

    $.ajax({
        type : "GET",
        contentType : "application/json",
        url : "adverts/getCategoriesById?id="+id,
        async: false,
        success : function(data) {
            console.log(data);
            for(var i = 0; i < data.length; i++)
                if ($("#categories").val() === "")
                    $("#categories").val(data[i].categoryName);
                else
                    $("#categories").val($("#categories").val()+';'+data[i].categoryName);
        },
        error : function(e) {
            alert("Error!");
            console.log("ERROR: ", e);
        }
    });
    
    $.ajax({
        type: "GET",
        contentType: "application/json",
        accept: 'text/plain',
        url: "/activeuser/findactiveuser",
        dataType: 'text',
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            
            $("a[href*='news']").css("display", "");
        	$("a[href*='games']").css("display", "");
        	$("a[href*='advertisements']").css("display", "");
        	$("a[href*='forum']").css("display", "");
        	$(".btn-primary").css("display","");
        	
        	if(JSON.parse(data).user_level < 4){
        		$("a[href*='registration']").css("display", "");
        		$("a[href*='login']").css("display", "");
        		$("a[onclick*='javascript: $.ajax({type: ']").css("display", "none");
        	}
        
        if(JSON.parse(data).user_level >= 4){
        	
        	$("a[href*='listofusers']").css("display", "");
       	 $("a[href*='profile']").css("display", "");
       	 $("a[onclick*='javascript: $.ajax({type: ']").css("display", "");
        }
        },
        error: function (e) {
            console.log(e);
        }
    });
}

function validate(data) {
    var badInput = 0;
    if (data.title.length < 2 || data.title.length > 40) {
        badInput++;
        $('#title').css("background", "#ffbfbf");
        $('#title').css("outline-color", "red");
        $('#title').css("outline-width", "thick");
    }
    else{
        $('#title').css("background", "white");
        $('#title').css("outline-color", "black");
        $('#title').css("outline-width", "thin");
    }
    if (data.description.length < 2 ){
        badInput++;
        $('#description').css("background", "#ffbfbf");
        $('#description').css("outline-color", "red");
        $('#description').css("outline-width", "thick");
    }
    else{
        $('#description').css("background", "white");
        $('#description').css("outline-color", "black");
        $('#description').css("outline-width", "thin");
    }
    if (data.imageLink.length < 2 ){
        badInput++;
        $('#image').css("background", "#ffbfbf");
        $('#image').css("outline-color", "red");
        $('#image').css("outline-width", "thick");
    }
    else{
        $('#image').css("background", "white");
        $('#image').css("outline-color", "black");
        $('#image').css("outline-width", "thin");
    }
    if (data.place.length < 2 || data.place.length > 40){
        badInput++;
        $('#place').css("background", "#ffbfbf");
        $('#place').css("outline-color", "red");
        $('#place').css("outline-width", "thick");
    }
    else{
        $('#place').css("background", "white");
        $('#place').css("outline-color", "black");
        $('#place').css("outline-width", "thin");
    }
    if (data.max_participants < 2 || data.max_participants == null){
        badInput++;
        $('#max_participants').css("background", "#ffbfbf");
        $('#max_participants').css("outline-color", "red");
        $('#max_participants').css("outline-width", "thick");
    }
    else{
        $('#max_participants').css("background", "white");
        $('#max_participants').css("outline-color", "black");
        $('#max_participants').css("outline-width", "thin");
    }
    if (data.event_date == null){
        badInput++;
        $('#event_date').css("background", "#ffbfbf");
        $('#event_date').css("outline-color", "red");
        $('#event_date').css("outline-width", "thick");
    }
    else{
        $('#event_date').css("background", "white");
        $('#event_date').css("outline-color", "black");
        $('#event_date').css("outline-width", "thin");
    }
    if(badInput > 0) return false;
    return true;
}

function submitAdvert(){
    var url = new URL(window.location.href);
    var advertId = url.searchParams.get("id");
    advertId = advertId != null ? advertId.replace('/save', '') : "none";

    var strCategories = $('#categories').val();
    var listCats = strCategories.split(";");

    var categories = [];
    for(var i = 0; i < listCats.length; i++){
        if (listCats[i].toString() === "") continue;
        if (listCats[i].replace(/\s\s+/g, '') === "") continue;
        var cat = {
            advertId : advertId,
            categoryName: listCats[i].toString(),
        };
        categories.push(cat);
    }

    console.log(advert);

    var advert = {};
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/activeuser/findactiveuser",
        cache: false,
        timeout: 600000,
        async: false,
        success: function (data) {
            console.log(data);
            advert = {
                title : $('#title').val(),
                description : $('#description').val(),
                place : $('#place').val(),
                imageLink : $('#image').val(),
                max_participants : $('#max_participants').val(),
                event_date : $('#event_date').val(),
                authorId: data.originalid,
            }
            if (!validate(advert)) {
                alert("Error! Neteisingai uzpildyti pazymeti laukai.");
                return;
            }
            $.ajax({
                type : "POST",
                contentType : "application/json",
                accept: 'text/plain',
                url : "adverts/save?id="+advertId,
                data : JSON.stringify(advert),
                dataType: 'text',
                id: advertId,
                async: false,
                success : function(result) {
                    //window.location.href="/advertisements"
                    // fill successfully message on view
                    console.log("geriau nebuna");
                    window.location.href = "/advertisements";
                    $.ajax({
                        type : "POST",
                        contentType : "application/json",
                        accept: 'text/plain',
                        url : "adverts/saveCategories?id="+advertId,
                        data : JSON.stringify(categories),
                        dataType: 'text',
                        async: false,
                        success : function(result) {
                            console.log(categories);
                            window.location.href="/advertisements";
                            // fill successfully message on view
                            console.log("geras nebuna");
                            alert("ahhhhhhh");
                        },
                        error : function(e) {
                            console.log(advertId);
                            console.log(strCategories);
                            console.log("ERROR: kategorijose - ", e);
                            alert("Error!")
                        }
                    });
                },
                error : function(e) {
                    console.log(advert);
                    alert("Error!")
                    console.log("ERROR: ", e);
                }
            });
        },
        error: function (e) {
            alert("Error!");
            console.log("ERROR: ", e);
        }
    });

    window.location.href="/advertisements"
    // DO POST
}

module.exports = {
    submitAdvert: submitAdvert,
};
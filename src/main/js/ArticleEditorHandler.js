$(document).ready(function () {
	var url = new URL(window.location.href);
	var id = url.searchParams.get("id");
	$.ajax({
        type: "GET",
        contentType: "application/json",
        accept: 'text/plain',
        url: "/activeuser/findactiveuser",
        dataType: 'text',
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            
            $("a[href*='news']").css("display", "");
        	$("a[href*='games']").css("display", "");
        	$("a[href*='advertisements']").css("display", "");
        	$("a[href*='forum']").css("display", "");
        	$(".btn-primary").css("display","");
        	
        	if(JSON.parse(data).user_level < 4){
        		$("a[href*='registration']").css("display", "");
        		$("a[href*='login']").css("display", "");
        		$("a[onclick*='javascript: $.ajax({type: ']").css("display", "none");
        	}
        
        if(JSON.parse(data).user_level >= 4){
        	
        	 $("a[href*='listofusers']").css("display", "");
        	 $("a[href*='profile']").css("display", "");
        	 $("a[onclick*='javascript: $.ajax({type: ']").css("display", "");
        	 
        }
        },
        error: function (e) {
            console.log(e);
        }
    });
	if (id != "" && id != null && typeof id != "undefined"){
		getSingleArticle(id);
	}
});

function getSingleArticle(id){
	$.ajax({
		type : "GET",
		contentType : "application/json",
		accept: 'text/plain',
		url : "articles/findById?id="+id,
		dataType: 'text',
		async: false,
		success : function(data) {
			 console.log(data);
			 $("#name").val(JSON.parse(data).name);
			 $("#tags").val(JSON.parse(data).tags);
			 $("#imageLink").val(JSON.parse(data).imageLink);
			 $("#sourceLink").val(JSON.parse(data).sourceLink);
			 $("#content").val(JSON.parse(data).content);
		 	$("#category").val(JSON.parse(data).category != null? JSON.parse(data).category[0].name : '');
		},
		error : function(e) {
			alert("Error!")
			console.log("ERROR: ", e);
		}
	});
	
	
}

function submitArticle(){
	var article = {
		name : $("#name").val(),
		tags : $("#tags").val(),
		imageLink : $("#imageLink").val(),
		sourceLink : $("#sourceLink").val(),
		content : $("#content").val()
	}
	if (article.name == null || article.name.trim() == "" ||
			article.tags == null || article.tags.trim() == "" ||
			article.imageLink == null || article.imageLink.trim() == "" ||
			article.sourceLink == null || article.sourceLink.trim() == "" ||
			article.content == null || article.content.trim() == "" ||
			$("#category").val() == null)
		{
			alert("yra klaidingai užpildytų laukų");
			return;
		}
	var url = new URL(window.location.href);
	var articleId = url.searchParams.get("id");
	articleId = articleId != null ? articleId.replace('/save', '') : "none";
	// DO POST
	$.ajax({
		type : "POST",
		contentType : "application/json",
		accept: 'text/plain',
		url : "articles/save?id="+articleId+"&category="+$("#category").val(),
		data : JSON.stringify(article),
		dataType: 'text',
		id: articleId,
		success : function(result) {
			if (articleId != "none" && articleId != null && articleId != "")
			{
				window.location.href="/article?id="+articleId;
			}
			else
			{
				window.location.href="/news";
			}
					
		},
		error : function(e) {
			alert("yra klaidingai užpildytų laukų");
			return;
		}
	});
}

module.exports = {
		submitArticle: submitArticle,
};
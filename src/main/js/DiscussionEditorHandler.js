$(document).ready(function () {
	var url = new URL(window.location.href);
	var id = url.searchParams.get("id");
	$.ajax({
        type: "GET",
        contentType: "application/json",
        accept: 'text/plain',
        url: "/activeuser/findactiveuser",
        dataType: 'text',
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            
            $("a[href*='news']").css("display", "");
        	$("a[href*='games']").css("display", "");
        	$("a[href*='advertisements']").css("display", "");
        	$("a[href*='forum']").css("display", "");
        	$(".btn-primary").css("display","");
        	
        	if(JSON.parse(data).user_level < 4){
        		$("a[href*='registration']").css("display", "");
        		$("a[href*='login']").css("display", "");
        		$("a[onclick*='javascript: $.ajax({type: ']").css("display", "none");
        	}
        
        if(JSON.parse(data).user_level >= 4){
        	
        	$("a[href*='listofusers']").css("display", "");
       	 $("a[href*='profile']").css("display", "");
       	 $("a[onclick*='javascript: $.ajax({type: ']").css("display", "");
        }
            
        },
        error: function (e) {
            console.log(e);
        }
    });
	if (id != "" && id != null && typeof id != "undefined"){
		getSingleDiscussion(id);
	}else{
		$('#commentButton').hide();
		$('#commentTable').hide();
	}
	$('#hiddenCommentId').hide();
});

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

function getSingleDiscussion(id){
	
	
	
	var activeUserName = '';
	var activeUserLevel = 0;
	
	$.ajax({
		type : "GET",
		contentType : "application/json",
		accept: 'text/plain',
		url : "activeuser/findactiveuser",
		dataType: 'text',
		async: false,
		success : function(data) {
			 var active = JSON.parse(data);
			 activeUserName = active.username;
			 activeUserLevel = active.user_level;
			 
			 $("a[href*='news']").css("display", "");
	        	$("a[href*='games']").css("display", "");
	        	$("a[href*='advertisements']").css("display", "");
	        	$("a[href*='forum']").css("display", "");
	        	$(".btn-primary").css("display","");
	        	
	        	if(JSON.parse(data).user_level < 4){
	        		$("a[href*='registration']").css("display", "");
	        		$("a[href*='login']").css("display", "");
	        		$("a[onclick*='javascript: $.ajax({type: ']").css("display", "none");
	        	}
	        
	        if(JSON.parse(data).user_level >= 4){
	        	
	        	$("a[href*='listofusers']").css("display", "");
	       	 $("a[href*='profile']").css("display", "");
	       	 $("a[onclick*='javascript: $.ajax({type: ']").css("display", "");
	        }
	            
	            if(JSON.parse(data).user_level >= 4){
	                $("#new_article").css("display", "inline");
	           }
			 
			 $.ajax({
					type : "GET",
					contentType : "application/json",
					accept: 'text/plain',
					url : "forums/findById?id="+id,
					dataType: 'text',
					async: false,
					success : function(data) {
						 $("#topic").val(JSON.parse(data).topic);
						 $("#description").val(JSON.parse(data).description);
						 
						 
						 if (JSON.parse(data).user[0].username != activeUserName && activeUserLevel <= 5){
							 $("#topic").attr('disabled', 'disabled');
							 $("#description").attr('disabled', 'disabled');
							 $('button[class*=primary]').hide();
						 }
						 
						 
						 for (var i = 0; i < JSON.parse(data).comments.length; i++){
							 var text = JSON.parse(data).comments[i].commentText;
							 var creationDate = JSON.parse(data).comments[i].commentDate;
							 var editDate = JSON.parse(data).comments[i].editDate;
							 var user = JSON.parse(data).comments[i].user != null ? JSON.parse(data).comments[i].user.username : '';
							 //REIKIA PASIIMT USERI
							 var editBtn = '';
							 var deleteBtn = '';
							 
							 if (activeUserLevel > 5 || activeUserName == user)
							 	deleteBtn = '<td><a href="javascript: ui.deleteComment(\''+JSON.parse(data).comments[i].id+'\')" class="btn btn-danger mt-auto">Šalinti</a>';
							 if (activeUserLevel > 5 || activeUserName == user)
							 	editBtn = '<a href="javascript: ui.editComment(\''+JSON.parse(data).comments[i].id+'\')" class="btn btn-positive mt-auto">Redaguoti</a>';
							 
							 
							 $('#commentTableBody').append("<tr><td>"+ user+"</td><td>"+formatDate(new Date(creationDate))+"</td><td>"+formatDate(new Date(editDate))+"</td><td>"+text+'</td>'+deleteBtn+'</td><td>'+editBtn+"</td></tr>");
							 
						 }
					},
					error : function(e) {
						alert("Error!")
						console.log("ERROR: ", e);
					}
				});
			 
		},
		error : function(e) {
			alert("Error!")
			console.log("ERROR: ", e);
		}
	});
}

function submitDiscussion(){
	$("#description").attr('style', '');
	$("#topic").attr('style', '');
	
	if ($("#topic").val() == ''){
		$("#topic").attr('style', 'background-color:red;opacity:0.5');
		return;
	}
	
	if ($("#description").val() == ''){
		$("#description").attr('style', 'background-color:red;opacity:0.5');
		return;
	}
	var discussion = {
		topic : $("#topic").val(),
		description : $("#description").val()
	}
	var url = new URL(window.location.href);
	var discussionId = url.searchParams.get("id");
	discussionId = discussionId != null ? discussionId.replace('/save', '') : "none";
	// DO POST
	$.ajax({
		type : "POST",
		contentType : "application/json",
		accept: 'text/plain',
		url : "forums/save?id="+discussionId,
		data : JSON.stringify(discussion),
		dataType: 'text',
		id: discussionId,
		success : function(result) {
			//to do redirect back and show success dialog
			window.location.href = "/forum";
		},
		error : function(e) {
			alert("Error!")
			console.log("ERROR: " + e);
		}
	});
}

function submitComment(){
	var url = new URL(window.location.href);
	var id = url.searchParams.get("id");
	
	$("#commentText").attr('style', '');
	
	if ($("#commentText").val() == ''){
		$("#commentText").attr('style', 'background-color:red;opacity:0.5');
		return;
	}
	
	var comment = {
		commentText : $("#commentText").val()
	}
	
	if ($('#hiddenCommentId').val() == ''){
		// DO POST
		$.ajax({
			type : "POST",
			contentType : "application/json",
			accept: 'text/plain',
			url : "forums/saveComment?id="+id,
			data : JSON.stringify(comment),
			dataType: 'text',
			success : function(result) {
				window.location.href = window.location.href;
			},
			error : function(e) {
				alert("Error!")
				console.log("ERROR: " + e);
			}
		});
	}else{
		// DO POST
		$.ajax({
			type : "POST",
			contentType : "application/json",
			accept: 'text/plain',
			url : "forums/editComment?id="+id+'&commid='+$('#hiddenCommentId').val(),
			data : JSON.stringify(comment),
			dataType: 'text',
			success : function(result) {
				window.location.href = window.location.href;
			},
			error : function(e) {
				alert("Error!")
				console.log("ERROR: " + e);
			}
		});
	}
}

function deleteComment(idas) {
	var url = new URL(window.location.href);
	var id = url.searchParams.get("id");
	
	
	$.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/forums/removeComment?id="+idas+"&disc="+id,
        cache: false,
        timeout: 600000,
        success: function (data) {
            window.location.href = window.location.href;
        },
        error: function (e) {
        }
    });
}

function editComment(commId){
	$('#hiddenCommentId').val(commId);
	$('#commentModal').modal();
}

function sortCommentsByEditedByDate(){
	var url = new URL(window.location.href);
	var id = url.searchParams.get("id");
	var activeUserName = '';
	var activeUserLevel = 0;
	
	$.ajax({
		type : "GET",
		contentType : "application/json",
		accept: 'text/plain',
		url : "activeuser/findactiveuser",
		dataType: 'text',
		async: false,
		success : function(data) {
			 var active = JSON.parse(data);
			 activeUserName = active.username;
			 activeUserLevel = active.user_level;
			 
			 
			 $('#commentTableBody').html('');
				$.ajax({
					type : "GET",
					contentType : "application/json",
					accept: 'text/plain',
					url : "forums/getSortedCommentsByEdited?id="+id,
					dataType: 'text',
					async: false,
					success : function(data) {
						 for (var i = 0; i < JSON.parse(data).length; i++){
							 var comment = JSON.parse(data)[i];
							 
							 var text = comment.commentText;
							 var creationDate = comment.commentDate;
							 var editDate = comment.editDate;
							 var user = comment.user != null ? comment.user.username : '';
							 var editBtn = '';
							 var deleteBtn = '';
							 
							 if (activeUserLevel > 5 || activeUserName == user)
							 	deleteBtn = '<td><a href="javascript: ui.deleteComment(\''+comment.id+'\')" class="btn btn-danger mt-auto">Šalinti</a>';
							 if (activeUserLevel > 5 || activeUserName == user)
							 	editBtn = '<a href="javascript: ui.editComment(\''+comment.id+'\')" class="btn btn-positive mt-auto">Redaguoti</a>';
							 $('#commentTableBody').append("<tr><td>"+user+"</td><td>"+formatDate(new Date(creationDate))+"</td><td>"+formatDate(new Date(editDate))+"</td><td>"+text+'</td>'+deleteBtn+'</td><td>'+editBtn+"</td></tr>"); 
						 }
					}
				});
		}
	});
}

function sortCommentsByCreatedDate(){
	var url = new URL(window.location.href);
	var id = url.searchParams.get("id");
	
	var activeUserName = '';
	var activeUserLevel = 0;
	
	$.ajax({
		type : "GET",
		contentType : "application/json",
		accept: 'text/plain',
		url : "activeuser/findactiveuser",
		dataType: 'text',
		async: false,
		success : function(data) {
			 var active = JSON.parse(data);
			 activeUserName = active.username;
			 activeUserLevel = active.user_level;
			 
			 
			 $('#commentTableBody').html('');
				$.ajax({
					type : "GET",
					contentType : "application/json",
					accept: 'text/plain',
					url : "forums/getSortedCommentsByEdited?id="+id,
					dataType: 'text',
					async: false,
					success : function(data) {
						 for (var i = 0; i < JSON.parse(data).length; i++){
							 var comment = JSON.parse(data)[i];
							 
							 var text = comment.commentText;
							 var creationDate = comment.commentDate;
							 var editDate = comment.editDate;
							 var user = comment.user != null ? comment.user.username : '';
							 var editBtn = '';
							 var deleteBtn = '';
							 
							 if (activeUserLevel > 5 || activeUserName == user)
							 	deleteBtn = '<td><a href="javascript: ui.deleteComment(\''+comment.id+'\')" class="btn btn-danger mt-auto">Šalinti</a>';
							 if (activeUserLevel > 5 || activeUserName == user)
							 	editBtn = '<a href="javascript: ui.editComment(\''+comment.id+'\')" class="btn btn-positive mt-auto">Redaguoti</a>';
							 $('#commentTableBody').append("<tr><td>"+user+"</td><td>"+formatDate(new Date(creationDate))+"</td><td>"+formatDate(new Date(editDate))+"</td><td>"+text+'</td>'+deleteBtn+'</td><td>'+editBtn+"</td></tr>"); 
						 }
					}
				});
		}
	});
}


module.exports = {
		submitDiscussion: submitDiscussion,
		submitComment: submitComment,
		deleteComment: deleteComment,
		editComment: editComment,
		sortCommentsByCreatedDate: sortCommentsByCreatedDate,
		sortCommentsByEditedByDate: sortCommentsByEditedByDate,
};
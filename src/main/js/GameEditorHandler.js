$(document).ready(function () {
	var url = new URL(window.location.href);
	var id = url.searchParams.get("id");
	$.ajax({
        type: "GET",
        contentType: "application/json",
        accept: 'text/plain',
        url: "/activeuser/findactiveuser",
        dataType: 'text',
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            
            $("a[href*='news']").css("display", "");
        	$("a[href*='games']").css("display", "");
        	$("a[href*='advertisements']").css("display", "");
        	$("a[href*='forum']").css("display", "");
        	$(".btn-primary").css("display","");
        	
        	if(JSON.parse(data).user_level < 4){
        		$("a[href*='registration']").css("display", "");
        		$("a[href*='login']").css("display", "");
        		$("a[onclick*='javascript: $.ajax({type: ']").css("display", "none");
        	}
        
        if(JSON.parse(data).user_level >= 4){
        	
        	$("a[href*='listofusers']").css("display", "");
       	 $("a[href*='profile']").css("display", "");
       	 $("a[onclick*='javascript: $.ajax({type: ']").css("display", "");
        }
            
        },
        error: function (e) {
            console.log(e);
        }
    });
	if (id != "" && id != null && typeof id != "undefined"){
		getSingleGame(id);
	}
});

function getDateFormat(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    var date = new Date();
    date.toLocaleDateString();

    return [year, month, day].join('-');
};

function getSingleGame(id){
	$.ajax({
		type : "GET",
        contentType : "application/json",
        accept: 'text/plain',
        url : "gamess/findById?id="+id,
        dataType: 'text',
        async: false,
		success : function(data) {
			console.log(data);
			 $("#name").val(JSON.parse(data).name);
			 $("#description").val(JSON.parse(data).description);
			 $("#photo").val(JSON.parse(data).photo);
             $("#place").val(JSON.parse(data).place);
             $("#event_date").val(getDateFormat(JSON.parse(data).event_date));
             $("#team_a").val(JSON.parse(data).team_a);
             $("#team_b").val(JSON.parse(data).team_b);
             $("#result_a").val(JSON.parse(data).result_a);
			 $("#result_b").val(JSON.parse(data).result_b);
			 $("#category").val(JSON.parse(data).category != null? JSON.parse(data).category[0].name : '');

		},
		error : function(e) {
			alert("Error!")
			console.log("ERROR: ", e);
		}
	});
	
	$.ajax({
        type: "GET",
        contentType: "application/json",
        accept: 'text/plain',
        url: "/activeuser/findactiveuser",
        dataType: 'text',
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            
            $("a[href*='news']").css("display", "");
        	$("a[href*='games']").css("display", "");
        	$("a[href*='advertisements']").css("display", "");
        	$("a[href*='forum']").css("display", "");
        	$(".btn-primary").css("display","");
        	
        	if(JSON.parse(data).user_level < 4){
        		$("a[href*='registration']").css("display", "");
        		$("a[href*='login']").css("display", "");
        		$("a[onclick*='javascript: $.ajax({type: ']").css("display", "none");
        	}
        
        if(JSON.parse(data).user_level >= 4){
        	
        	$("a[href*='listofusers']").css("display", "");
       	 $("a[href*='profile']").css("display", "");
       	 $("a[onclick*='javascript: $.ajax({type: ']").css("display", "");
        }
            
        },
        error: function (e) {
            console.log(e);
        }
    });
}

function submitGame(){
	var game = {
		name : $("#name").val(),
		description : $("#description").val(),
		photo: $("#photo").val(),
        place: $("#place").val(),
        event_date: $("#event_date").val(),
        team_a: $("#team_a").val(),
        team_b: $("#team_b").val(),
        result_a: 0,
		result_b: 0,
	}
	var url = new URL(window.location.href);
	var gameId = url.searchParams.get("id");
    gameId = gameId != null ? gameId.replace('/save', '') : "none";
	// DO POST
    if($("#event_date").val() !== null || $("#event_date").val() === "")
    	{
	$.ajax({
		type : "POST",
		contentType : "application/json",
		accept: 'text/plain',
		url : "gamess/save?id="+gameId+"&category="+$("#category").val(),
		data : JSON.stringify(game),
		dataType: 'text',
		id: gameId,
		success : function(result){
			alert("Sėkmingai sukurtas/redaguotas varžybų įrašas")
			window.location.href="/games?id="+gameId;
			
		},
		error : function(e) {
			
			window.location.href="/games"
		}
	});
    }
    else{
    	alert("Nepilnai užpildyti laukai!")
    	
    }
}

function submitResult(){
	var game = {
		result_a : $("#result_a").val(),
		result_b : $("#result_b").val(),
	}

	var url = new URL(window.location.href);
	var gameId = url.searchParams.get("id");
    gameId = gameId != null ? gameId.replace('/saveResult', '') : "none";
	// DO POST
	$.ajax({
		type : "POST",
		contentType : "application/json",
		accept: 'text/plain',
		url : "gamess/saveResult?id="+gameId,
		data : JSON.stringify(game),
		dataType: 'text',
		id: gameId,
		success : function(result){
			alert("Pakeitėte rezultatus sėkmingai");
				window.location.href="/games?id="+gameId;
		},
		error : function(e) {
			console.log(game)
			alert("Error!" + e)
			console.log("ERROR: " + e);
		}
	});
}

module.exports = {
		submitGame: submitGame,
		submitResult: submitResult,
};
'use strict';
const React = require('react');
const ReactDOM = require('react-dom');
import scrollToComponent from 'react-scroll-to-component';
import 'bootstrap/dist/css/bootstrap.css';
import './assets/styles/styles.css'
import ReactModal from 'react-modal';

const follow = require('./follow'); // function to hop multiple links by "rel"

const root = '/api';

const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)'
    }
};

class App extends React.Component {

    constructor () {
        super();
    }

    render() {
        return (
        		<div> Hello world </div>
        );

    }
}

ReactDOM.render(
		<App />,
		document.getElementById('react')
)


$(document).ready(function () {
    console.log("here");
    var url = new URL(window.location.href);
    var id = url.searchParams.get("id");
    $.ajax({
        type: "GET",
        contentType: "application/json",
        accept: 'text/plain',
        url: "/activeuser/findactiveuser",
        dataType: 'text',
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            
            $("a[href*='news']").css("display", "");
        	$("a[href*='games']").css("display", "");
        	$("a[href*='advertisements']").css("display", "");
        	$("a[href*='forum']").css("display", "");
        	$(".btn-primary").css("display","");
        	
        	if(JSON.parse(data).user_level < 4){
        		$("a[href*='registration']").css("display", "");
        		$("a[href*='login']").css("display", "");
        		$("a[onclick*='javascript: $.ajax({type: ']").css("display", "none");
        	}
        
        if(JSON.parse(data).user_level >= 4){
        	
        	$("a[href*='listofusers']").css("display", "");
       	 $("a[href*='profile']").css("display", "");
       	 $("a[onclick*='javascript: $.ajax({type: ']").css("display", "");
        }
            
        },
        error: function (e) {
            console.log(e);
        }
    });
    if (id != "" && id != null && typeof id != "undefined"){
        fire_ajax_submit(id);
    }
});

function getDateFormat(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    var date = new Date();
    date.toLocaleDateString();

    return [year, month, day].join('-');
};

function fire_ajax_submit(id) {

    var advertAuthor = "";
    var userDuomenys = {};
    $.ajax({
        type : "GET",
        contentType : "application/json",
        accept: 'text/plain',
        url : "adverts/findById?id="+id,
        dataType: 'text',
        async: false,
        success : function(data) {
            console.log(data);
            advertAuthor = JSON.parse(data).authorId;
            userDuomenys = { advertId: JSON.parse(data).id };
            $(".advert-data")
                .append('' +
                    '<tr><td><b>Pavadinimas: </b>'+JSON.parse(data).title+'</td></tr>' +
                    '<tr><td><img id="imageLinkas" src="'+JSON.parse(data).imageLink+'" alt="Nuotrauka"/></td></tr>' +
                    '<tr><td><b>Aprašymas: </b>'+JSON.parse(data).description+'</td></tr>' +
                    '<tr><td><b>Vieta: </b>'+JSON.parse(data).place+'</td></tr>'+
                    '<tr><td><b>Dalyvių skaičius: </b>'+JSON.parse(data).max_participants+'</td></tr>'+
                    '<tr><td><b>Veiksmo data: </b>'+getDateFormat(JSON.parse(data).event_date)+'</td></tr>' );
            $(".advert-buttons")
                .append('' +
                    '<a href="javascript: ui.addLike(\''+JSON.parse(data).id+'\')" id="advert-likeY-btn" class="btn btn-primary mr-2 mt-auto">Like</a>' +
                    '<a href="javascript: ui.removeLike(\''+JSON.parse(data).id+'\')" id="advert-likeN-btn" class="btn btn-primary mr-2 mt-auto">Unlike</a>' +
                    '<a href="javascript: ui.addParticipant(\''+JSON.parse(data).id+'\')" id="advert-participantY-btn" class="btn btn-outline-primary mr-2 mt-auto">Dalyvausiu!</a>' +
                    '<a href="javascript: ui.deleteParticipant(\''+JSON.parse(data).id+'\')" id="advert-participantN-btn" class="btn btn-outline-primary mr-2 mt-auto">Nedalyvausiu!</a>' +
                    '<a href="/editAdvert?id='+JSON.parse(data).id+'" id="advert-edit-btn" class="btn btn-primary mr-2 mt-auto">Redaguoti</a>' +
                    '<a href="javascript: ui.deleteAdvert(\''+JSON.parse(data).id+'\')" id="advert-delete-btn" class="btn btn-danger mr-2 mt-auto">Šalinti</a>'+
                    '<a href="https://www.facebook.com/sharer/sharer.php?u='+window.location.href+'&t='+JSON.parse(data).title+'" target="_blank" ' +
                    'onclick="window.open(this.href,\'targetWindow\',\'toolbar=no,location=0,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=250\'); return false">' +
                    '<button class="btn btn-facebook mr-2"> Dalintis Facebook </button> </a>')
            },
        error : function(e) {
            alert("Error!");
            console.log("ERROR: ", e);
        }
    });

    var ulras = new URL(window.location.href);
    var advertIdas = ulras.searchParams.get("id");

    $.ajax({
        type : "GET",
        contentType : "application/json",
        accept: 'text/plain',
        url : "adverts/getAllLikesById?id="+advertIdas,
        dataType: 'text',
        id: advertIdas,
        async: false,
        success : function(data) {
            console.log(data);
            $(".advert-data")
                .append('' +
                    '<tr><td id="advert-data-laikai"><b>Patinka: </b>'+JSON.parse(data).length+' <b>naudotojų</b></td></tr>');
        },
        error : function(e) {
            alert("Error!");
            console.log("ERROR: ", e);
        }
    });

    $.ajax({
        type : "GET",
        contentType : "application/json",
        accept: 'text/plain',
        url : "adverts/getAllParticipantsById?id="+advertIdas,
        dataType: 'text',
        id: advertIdas,
        async: false,
        success : function(data) {
            console.log(data);
            $(".advert-data")
                .append('' +
                    '<tr><td id="advert-data-dalyviai"><b>Dalyvių sk.: </b>'+JSON.parse(data).length+'</td></tr>');
        },
        error : function(e) {
            alert("Error!");
            console.log("ERROR: ", e);
        }
    });

    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/activeuser/findactiveuser",
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            var url = new URL(window.location.href);
            var advertId = url.searchParams.get("id");
            userDuomenys = {
                user_level: data.user_level,
                id: data.originalid,
                advertId: advertId,
            };
            
            $("a[href*='news']").css("display", "");
        	$("a[href*='games']").css("display", "");
        	$("a[href*='advertisements']").css("display", "");
        	$("a[href*='forum']").css("display", "");
        	$(".btn-primary").css("display","");
        	
        	if(JSON.parse(data).user_level < 4){
        		$("a[href*='registration']").css("display", "");
        		$("a[href*='login']").css("display", "");
        		$("a[onclick*='javascript: $.ajax({type: ']").css("display", "none");
        	}
        
        if(JSON.parse(data).user_level >= 4){
        	
        	$("a[href*='listofusers']").css("display", "");
       	 $("a[href*='profile']").css("display", "");
       	 $("a[onclick*='javascript: $.ajax({type: ']").css("display", "");
        }
            
            if ((data.user_level < 6 || data.user_level === null) && data.originalid !== advertAuthor ){
                $("#advert-edit-btn").css("display", "none");
                $("#advert-delete-btn").css("display", "none");
            }
            if (data.originalid === advertAuthor){
                $("#advert-edit-btn").css("display", "inline");
                $("#advert-delete-btn").css("display", "inline");
            }

            var adLike = {
                advertId : userDuomenys.advertId,
                userId : userDuomenys.id,
            };
            $.ajax({
                accept: 'text/plain',
                type: "POST",
                data : JSON.stringify(adLike),
                dataType: 'text',
                contentType: "application/json",
                url: "/adverts/getLikeById",
                cache: false,
                id: userDuomenys.advertId,
                uid: userDuomenys.id,
                timeout: 600000,
                success: function (data) {
                    console.log(userDuomenys);

                    console.log(data);
                    if (JSON.parse(data).id === null){
                        $("#advert-likeN-btn").css("display", "none");
                    }
                    else{
                        $("#advert-likeY-btn").css("display", "none");
                    }
                    if ((data.user_level < 4 || data.user_level === null)){
                        $("#advert-likeN-btn").css("display", "none");
                        $("#advert-likeY-btn").css("display", "none");
                    }

                    var adPart = {
                        advertId : userDuomenys.advertId,
                        participantId : userDuomenys.id,
                    };
                    console.log(adPart);
                    $.ajax({
                        accept: 'text/plain',
                        type: "POST",
                        data : JSON.stringify(adPart),
                        dataType: 'text',
                        contentType: "application/json",
                        url: "/adverts/getParticipantById",
                        cache: false,
                        timeout: 600000,
                        success: function (data) {
                            console.log(userDuomenys);
                            console.log(data);
                            if (JSON.parse(data).id === null){
                                $("#advert-participantN-btn").css("display", "none");
                            }
                            else {
                                $("#advert-participantY-btn").css("display", "none");
                            }
                            if ((data.user_level < 4 || data.user_level === null)){
                                $("#advert-participantN-btn").css("display", "none");
                                $("#advert-participantY-btn").css("display", "none");
                            }
                        },
                        error: function (e) {
                            $("#advert-likeN-btn").css("display", "none");
                            $("#advert-likeY-btn").css("display", "none");
                            $("#advert-participantN-btn").css("display", "none");
                            $("#advert-participantY-btn").css("display", "none");
                            $("#advert-edit-btn").css("display", "none");
                            $("#advert-delete-btn").css("display", "none");
                            //alert("Error!");
                            console.log("ERROR: ", e);
                        }});
                },
                error: function (e) {
                    $("#advert-likeN-btn").css("display", "none");
                    $("#advert-likeY-btn").css("display", "none");
                    $("#advert-participantN-btn").css("display", "none");
                    $("#advert-participantY-btn").css("display", "none");
                    $("#advert-edit-btn").css("display", "none");
                    $("#advert-delete-btn").css("display", "none");
                    //alert("Error!");
                    console.log("ERROR: ", e);
                }
            });

            },
        error: function (e) {
            $("#advert-likeN-btn").css("display", "none");
            $("#advert-likeY-btn").css("display", "none");
            $("#advert-participantN-btn").css("display", "none");
            $("#advert-participantY-btn").css("display", "none");
            $("#advert-edit-btn").css("display", "none");
            $("#advert-delete-btn").css("display", "none");
            //alert("Error!");
            console.log("ERROR: ", e);
        }
    });

    $.ajax({
        type : "GET",
        contentType : "application/json",
        url : "adverts/getCategoriesById?id="+id,
        async: false,
        success : function(data) {
            console.log(data);
            $(".advert-data")
                .append('<tr><td class="kat-klase"><b>Kategorijos: </b>');
            if (data.length > 1) {
                for (var i = 0; i < data.length; i++) {
                    $(".kat-klase")
                        .append('' + data[i].categoryName + '<b> | </b>');
                }
            } else {
                for (var i = 0; i < data.length; i++) {
                    $(".kat-klase")
                        .append('' + data[i].categoryName + '');
                }
            }
            ;
            $(".advert-data")
                .append('</td></tr>');
        },
        error : function(e) {
            alert("Error!");
            console.log("ERROR: ", e);
        }
    });
}

function removeLike(aId) {
    var url = new URL(window.location.href);
    var advertId = url.searchParams.get("id");
    aId = aId != null ? aId.replace('/removeAdvertLike', '') : "none";
    var uId = url.searchParams.get("pId");

    var userDuomenys = {};
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/activeuser/findactiveuser",
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            var url = new URL(window.location.href);
            var advertId = url.searchParams.get("id");
            userDuomenys = {
                user_level: data.user_level,
                id: data.originalid,
                advertId: advertId,
            };
        },
        error: function (e) {
            alert("Error!");
            console.log("ERROR: ", e);
        },
        complete: function(data) {
            $.ajax({
                id: aId,
                uid: userDuomenys.id,
                type: "POST",
                contentType: "application/json",
                url: "/adverts/removeAdvertLike?id=" + aId + "&uid=" + userDuomenys.id,
                cache: false,
                timeout: 600000,
                success: function (data) {
                    console.log("Advert like removed");
                    $("#advert-likeN-btn").css("display", "none");
                    $("#advert-likeY-btn").css("display", "inline");
                    //window.location.href = "/advertisements";
                },
                error: function (e) {
                    console.log("Error: " + e);
                },
                complete: function(data){
                    $.ajax({
                        type : "GET",
                        contentType : "application/json",
                        accept: 'text/plain',
                        url : "adverts/getAllLikesById?id="+aId,
                        dataType: 'text',
                        id: aId,
                        async: false,
                        success : function(data) {
                            console.log(data);
                            $("#advert-data-laikai").text("");
                            $("#advert-data-laikai")
                                .append( '<b>Patinka: </b>'+JSON.parse(data).length+'<b> naudotojų</b>');
                        },
                        error : function(e) {
                            alert("Error!");
                            console.log("ERROR: ", e);
                        }
                    });
                }
            });
        }
    });
}


function addLike(aId) {
    var adLike = {
        advertId : aId,
        userId : "0",
        dateOfLike : "0",
    }
    var url = new URL(window.location.href);
    var advertId = url.searchParams.get("id");
    aId = aId != null ? aId.replace('/addAdvertLike', '') : "none";
    var uId = url.searchParams.get("pId");

    var userDuomenys = {};
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/activeuser/findactiveuser",
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            var url = new URL(window.location.href);
            var advertId = url.searchParams.get("id");
            userDuomenys = {
                user_level: data.user_level,
                id: data.originalid,
                advertId: advertId,
            };
        },
        error: function (e) {
            alert("Error!");
            console.log("ERROR: ", e);
        },
        complete: function(data) {
            adLike = {
                advertId: aId,
                userId: userDuomenys.id,
            };
            console.log(userDuomenys);
            $.ajax({
                accept: 'text/plain',
                data: JSON.stringify(adLike),
                dataType: 'text',
                id: aId,
                uid: userDuomenys.id,
                type: "POST",
                contentType: "application/json",
                url: "/adverts/addAdvertLike?id=" + aId + "&uid="+ userDuomenys.id,
                cache: false,
                timeout: 600000,
                success: function (data) {
                    console.log("Advert like added");
                    $("#advert-likeN-btn").css("display", "inline");
                    $("#advert-likeY-btn").css("display", "none");

                    //window.location.href = "/advertisements";
                },
                error: function (e) {
                    console.log("Error: " + e);
                },
                complete: function(data){
                    $.ajax({
                        type : "GET",
                        contentType : "application/json",
                        accept: 'text/plain',
                        url : "adverts/getAllLikesById?id="+aId,
                        dataType: 'text',
                        id: aId,
                        async: false,
                        success : function(data) {
                            console.log(data);
                            $("#advert-data-laikai").text("");
                            $("#advert-data-laikai")
                                .append( '<b>Patinka: </b>'+JSON.parse(data).length+' <b>naudotojų</b>');
                        },
                        error : function(e) {
                            alert("Error!");
                            console.log("ERROR: ", e);
                        }
                    });
                }
            });
        }
    });
}


function addParticipant(aId) {
    var adPart = {
        advertId : aId,
        participantId : "0",
        registerDate : "0",
    }
    var url = new URL(window.location.href);
    var advertId = url.searchParams.get("id");
    aId = aId != null ? aId.replace('/addParticipant', '') : "none";
    var pId = url.searchParams.get("pId");

    var userDuomenys = {};
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/activeuser/findactiveuser",
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            var url = new URL(window.location.href);
            var advertId = url.searchParams.get("id");
            userDuomenys = {
                user_level: data.user_level,
                id: data.originalid,
                advertId: advertId,
            };
        },
        error: function (e) {
            alert("Error!");
            console.log("ERROR: ", e);
        },
        complete: function(data) {
            adPart = {
                advertId : aId,
                participantId : userDuomenys.id,
            }
            $.ajax({
                accept: 'text/plain',
                data: JSON.stringify(adPart),
                dataType: 'text',
                id: advertId,
                pid: userDuomenys.id,
                type: "POST",
                contentType: "application/json",
                url: "/adverts/addParticipant?id=" + aId + "&pid=" + userDuomenys.id,
                cache: false,
                timeout: 600000,
                success: function (data) {
                    $("#advert-participantN-btn").css("display", "inline");
                    $("#advert-participantY-btn").css("display", "none");
                    console.log("Advert participant added");
                    //window.location.href = "/advertisements";
                },
                error: function (e) {
                    console.log("Error: " + e);
                },
                complete: function(data){
                    $.ajax({
                        type : "GET",
                        contentType : "application/json",
                        accept: 'text/plain',
                        url : "adverts/getAllParticipantsById?id="+aId,
                        dataType: 'text',
                        id: aId,
                        async: false,
                        success : function(data) {
                            console.log(data);
                            $("#advert-data-dalyviai").text("");
                            $("#advert-data-dalyviai")
                                .append( '<b>Dalyvių sk.: </b>'+JSON.parse(data).length);
                        },
                        error : function(e) {
                            alert("Error!");
                            console.log("ERROR: ", e);
                        }
                    });
                }
            });
        }
    });
}

function deleteParticipant(aId) {
    var url = new URL(window.location.href);
    var advertId = url.searchParams.get("id");
    aId = aId != null ? aId.replace('/removeParticipant', '') : "none";
    var pId = url.searchParams.get("pId");

    var userDuomenys = {};
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/activeuser/findactiveuser",
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            var url = new URL(window.location.href);
            var advertId = url.searchParams.get("id");
            userDuomenys = {
                user_level: data.user_level,
                id: data.originalid,
                advertId: advertId,
            };
        },
        error: function (e) {
            alert("Error!");
            console.log("ERROR: ", e);
        },
        complete: function(data) {
            $.ajax({
                id: aId,
                pid: userDuomenys.id,
                type: "POST",
                contentType: "application/json",
                url: "/adverts/removeParticipant?id=" + aId + "&pid=" + userDuomenys.id,
                cache: false,
                timeout: 600000,
                success: function (data) {
                    $("#advert-participantN-btn").css("display", "none");
                    $("#advert-participantY-btn").css("display", "inline");
                    console.log("Advert participant removed");
                    //window.location.href = "/advertisements";
                },
                error: function (e) {
                    console.log("Error: " + e);
                },
                complete: function(data){
                    $.ajax({
                        type : "GET",
                        contentType : "application/json",
                        accept: 'text/plain',
                        url : "adverts/getAllParticipantsById?id="+aId,
                        dataType: 'text',
                        id: aId,
                        async: false,
                        success : function(data) {
                            console.log(data);
                            $("#advert-data-dalyviai").text("");
                            $("#advert-data-dalyviai")
                                .append( '<b>Dalyvių sk.: </b>'+JSON.parse(data).length);
                        },
                        error : function(e) {
                            alert("Error!");
                            console.log("ERROR: ", e);
                        }
                    });
                }
            });
        }
    });
}

function deleteAdvert(idas) {
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/adverts/removeAdvert?id="+idas,
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log("Advert removed");
            window.location.href = "/advertisements";
        },
        error: function (e) {
            console.log("Error: " + e);
        }
    });
}

function searchAdverts() {
    var searchQuery = $("#searchQuery").val();
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/adverts/findByName?query="+searchQuery,
        cache: false,
        async: false,
        timeout: 600000,
        success: function (data) {
            $(".advert-list").html("");
            for (var i = 0; i < data.length; i++){
                $(".advert-list")
                    .append('' +
                        '<tr> ' +
                        '<td><a href="/advert"'+data[i].title+'>'+data[i].title+'</a></td>' +
                        '<td>'+data[i].place+'</td>' +
                        '<td><button type="button" class="btn btn-danger btn-xtra" data-id="'+data[i].id+'" data-toggle="modal" class="open-deleteDialog" data-target="#deleteModal">X</button></td>' +
                        '<td><a href="/editAdvert?id='+data[i].id+'" class="btn btn-primary mt-auto">Redaguoti</a></td>' +
                        '<td><a href="javascript: ui.deleteAdvert(\''+data[i].id+'\')" class="btn btn-danger mt-auto">Šalinti</a></td>' +
                        ' </tr>>');
            }
        },
        error: function (e) {
        }
    });
}

module.exports = {
    deleteAdvert: deleteAdvert,
    searchAdverts: searchAdverts,
    addParticipant: addParticipant,
    deleteParticipant: deleteParticipant,
    addLike: addLike,
    removeLike: removeLike,
};
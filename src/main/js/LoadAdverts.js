$(document).ready(function () {

    fire_ajax_submit();


});

function getDateFormat(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    var date = new Date();
    date.toLocaleDateString();

    return [year, month, day].join('-');
};

function fire_ajax_submit() {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/adverts",
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            for (var i = 0; i < data.length; i++){
                if (data[i].event_date+(60*60*24) < $.now()) continue;
                $(".advert-list")
                    .append('' +
                        '<tr> ' +
                        '<td><a href="/openAdvert?id='+data[i].id+'">'+data[i].title+'</a></td>' +
                        '<td>'+data[i].place+'</td>' +
                        '<td>'+data[i].max_participants+'</td>'+
                        '<td>'+getDateFormat(data[i].event_date)+'</td>'+
                        '<td>'+getDateFormat(data[i].creation_date)+'</td>'+
                        '</tr>>');
            }

        },
        error: function (e) {
            console.log("Error: " + e);

        }
    });
    
    $.ajax({
        type: "GET",
        contentType: "application/json",
        accept: 'text/plain',
        url: "/activeuser/findactiveuser",
        dataType: 'text',
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            
            $("a[href*='news']").css("display", "");
        	$("a[href*='games']").css("display", "");
        	$("a[href*='advertisements']").css("display", "");
        	$("a[href*='forum']").css("display", "");
        	$(".btn-primary").css("display","");
        	
        	if(JSON.parse(data).user_level < 4){
        		$("a[href*='registration']").css("display", "");
        		$("a[href*='login']").css("display", "");
        		$("a[onclick*='javascript: $.ajax({type: ']").css("display", "none");
        	}
        
        if(JSON.parse(data).user_level >= 4){
        	
        	$("a[href*='listofusers']").css("display", "");
       	 $("a[href*='profile']").css("display", "");
       	 $("a[onclick*='javascript: $.ajax({type: ']").css("display", "");
        }
            
        },
        error: function (e) {
            console.log(e);
        }
    });

}

function sortAdvertsByCreationDateAsc(){
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/adverts/getAdvertsSortedByCreationDateAsc",
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            $(".advert-list").html("");
            loadSearchBar();
            for (var i = 0; i < data.length; i++){
                if (data[i].event_date+(60*60*24) < $.now()) continue;
                $(".advert-list")
                    .append('' +
                        '<tr> ' +
                        '<td><a href="/openAdvert?id='+data[i].id+'">'+data[i].title+'</a></td>' +
                        '<td>'+data[i].place+'</td>' +
                        '<td>'+data[i].max_participants+'</td>'+
                        '<td>'+getDateFormat(data[i].event_date)+'</td>'+
                        '<td>'+getDateFormat(data[i].creation_date)+'</td>'+
                        '</tr>>');
            }
        },
        error: function (e) {
            console.log("Error: " + e);
        }
    });
}

function sortAdvertsByCreationDateDesc(){
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/adverts/getAdvertsSortedByCreationDateDesc",
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            $(".advert-list").html("");
            loadSearchBar();
            for (var i = 0; i < data.length; i++){
                if (data[i].event_date+(60*60*24) < $.now()) continue;
                $(".advert-list")
                    .append('' +
                        '<tr> ' +
                        '<td><a href="/openAdvert?id='+data[i].id+'">'+data[i].title+'</a></td>' +
                        '<td>'+data[i].place+'</td>' +
                        '<td>'+data[i].max_participants+'</td>'+
                        '<td>'+getDateFormat(data[i].event_date)+'</td>'+
                        '<td>'+getDateFormat(data[i].creation_date)+'</td>'+
                        '</tr>>');
            }
        },
        error: function (e) {
            console.log("Error: " + e);
        }
    });
}

function sortAdvertsByEventDateAsc(){
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/adverts/getAdvertsSortedByEventDateAsc",
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            $(".advert-list").html("");
            loadSearchBar();
            for (var i = 0; i < data.length; i++){
                if (data[i].event_date+(60*60*24) < $.now()) continue;
                $(".advert-list")
                    .append('' +
                        '<tr> ' +
                        '<td><a href="/openAdvert?id='+data[i].id+'">'+data[i].title+'</a></td>' +
                        '<td>'+data[i].place+'</td>' +
                        '<td>'+data[i].max_participants+'</td>'+
                        '<td>'+getDateFormat(data[i].event_date)+'</td>'+
                        '<td>'+getDateFormat(data[i].creation_date)+'</td>'+
                        '</tr>>');
            }
        },
        error: function (e) {
            console.log("Error: " + e);
        }
    });
}

function sortAdvertsByEventDateDesc(){
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/adverts/getAdvertsSortedByEventDateDesc",
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            $(".advert-list").html("");
            loadSearchBar();
            for (var i = 0; i < data.length; i++){
                if (data[i].event_date+(60*60*24) < $.now()) continue;
                $(".advert-list")
                    .append('' +
                        '<tr> ' +
                        '<td><a href="/openAdvert?id='+data[i].id+'">'+data[i].title+'</a></td>' +
                        '<td>'+data[i].place+'</td>' +
                        '<td>'+data[i].max_participants+'</td>'+
                        '<td>'+getDateFormat(data[i].event_date)+'</td>'+
                        '<td>'+getDateFormat(data[i].creation_date)+'</td>'+
                        '</tr>>');
            }
        },
        error: function (e) {
            console.log("Error: " + e);
        }
    });
}

function sortAdvertsByTitleAsc(){
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/adverts/getAdvertsSortedByTitleAsc",
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            $(".advert-list").html("");
            loadSearchBar();
            for (var i = 0; i < data.length; i++){
                if (data[i].event_date+(60*60*24) < $.now()) continue;
                $(".advert-list")
                    .append('' +
                        '<tr> ' +
                        '<td><a href="/openAdvert?id='+data[i].id+'">'+data[i].title+'</a></td>' +
                        '<td>'+data[i].place+'</td>' +
                        '<td>'+data[i].max_participants+'</td>'+
                        '<td>'+getDateFormat(data[i].event_date)+'</td>'+
                        '<td>'+getDateFormat(data[i].creation_date)+'</td>'+
                        '</tr>>');
            }
        },
        error: function (e) {
            console.log("Error: " + e);
        }
    });
}

function sortAdvertsByTitleDesc(){
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/adverts/getAdvertsSortedByTitleDesc",
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            $(".advert-list").html("");
            loadSearchBar();
            for (var i = 0; i < data.length; i++){
                if (data[i].event_date+(60*60*24) < $.now()) continue;
                $(".advert-list")
                    .append('' +
                        '<tr> ' +
                        '<td><a href="/openAdvert?id='+data[i].id+'">'+data[i].title+'</a></td>' +
                        '<td>'+data[i].place+'</td>' +
                        '<td>'+data[i].max_participants+'</td>'+
                        '<td>'+getDateFormat(data[i].event_date)+'</td>'+
                        '<td>'+getDateFormat(data[i].creation_date)+'</td>'+
                        '</tr>>');
            }
        },
        error: function (e) {
            console.log("Error: " + e);
        }
    });
}


function deleteAdvert(idas) {
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/adverts/removeAdvert?id="+idas,
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log("Advert removed");
            window.location.href = "/advertisements";
        },
        error: function (e) {
            console.log("Error: " + e);
        }
    });
}

function loadSearchBar(){
    $(".advert-list")
        .append('' +
    '<tr>'+
        '<td><input type="text" id="titleQuery"></input></td>'+
        '<td><input type="text" id="placeQuery"></input></td>'+
        '<td><input type="number" id="maxPQuery"></input></td>'+
        '<td><input type="date" id="eventQuery"></input></td>'+
        '<td><input type="date" id="creationQuery"></input></td>'+
        '<td><a className="btn btn-default" href="javascript: ui.searchAdverts()" role="button">Ieškoti</a></td>'+
   '</tr>');
}

function searchAdverts() {
    /*var titleQuery = $("#titleQuery").val();
    var placeQuery = $("#placeQuery").val();
    var maxPQuery = $("#maxPQuery").val();
    var eventQuery = $("#eventQuery").val();
    var creationQuery = $("#creationQuery").val();
*/

    var advert = {
        title : $("#titleQuery").val(),
        place : $("#placeQuery").val(),
        creation_date : $("#creationQuery").val(),
        max_participants : $("#maxPQuery").val(),
        event_date : $("#eventQuery").val(),
    };

    console.log(advert);

    $.ajax({
        type : "POST",
        contentType : "application/json",
        url: "/adverts/findByQuery",
        data : JSON.stringify(advert),
        async: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            $(".advert-list").html("");
            loadSearchBar();
            for (var i = 0; i < data.length; i++){
                if (data[i] === null) continue;
                console.log(data[i]);
                if (data[i].event_date+(60*60*24) < $.now()) continue;
                $(".advert-list")
                    .append('' +
                        '<tr> ' +
                        '<td><a href="/openAdvert?id='+data[i].id+'">'+data[i].title+'</a></td>' +
                        '<td>'+data[i].place+'</td>' +
                        '<td>'+data[i].max_participants+'</td>'+
                        '<td>'+getDateFormat(data[i].event_date)+'</td>'+
                        '<td>'+getDateFormat(data[i].creation_date)+'</td>'+
                        '</tr>>');
            }
        },
        error: function (e) {
        }
    });
}

module.exports = {
    deleteAdvert: deleteAdvert,
    searchAdverts: searchAdverts,
    sortAdvertsByCreationDateAsc: sortAdvertsByCreationDateAsc,
    sortAdvertsByCreationDateDesc: sortAdvertsByCreationDateDesc,
    sortAdvertsByTitleAsc: sortAdvertsByTitleAsc,
    sortAdvertsByTitleDesc: sortAdvertsByTitleDesc,
    sortAdvertsByEventDateDesc: sortAdvertsByEventDateDesc,
    sortAdvertsByEventDateAsc: sortAdvertsByEventDateAsc,
};
$(document).ready(function () {

        fire_ajax_submit();


});

function getDateFormat(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
    	hours = d.getHours(),
    	minutes = d.getMinutes();
    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    if (hours < 10){
    	hours = '0' + hours;
    }
    if (minutes.length < 10)
    	minutes = '0' + minutes;
    var date = new Date();
    date.toLocaleDateString();

    return [year, month, day].join('-') + " " + [hours, minutes].join(':');
};

function fire_ajax_submit() {

    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/articles",
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            for (var i = 0; i < data.length; i++){
                var name = data[i].name;
                $(".card-deck").append('<div class="card mt-4" style="min-width: 20rem; "><img class="card-img-top" style="width: 100%; height: 15vw; object-fit: cover;" src="'+data[i].imageLink+'" alt="Card image cap"/> <div class="card-body d-flex flex-column"> <h4 class="card-title text-center">' +data[i].name + '</h4> <p class="card-text text-center text-secondary"> <span class="font-weight-bold">Kategorija</span> '+data[i].category[0].name+'<br> <span class="font-weight-bold">Atnaujinimo laikas: </span>'+ getDateFormat(data[i].update_date) +'<br> <span class="font-weight-bold">Žymės: </span>'+ data[i].tags +' </p> <a href="/article?id=' + data[i].id + '" class="btn btn-light mt-auto ">Skaityti plačiau...</a> </div> </div>');
            }

        },
        error: function (e) {
            console.log(e);
        }
    });
    
    $.ajax({
        type: "GET",
        contentType: "application/json",
        accept: 'text/plain',
        url: "/activeuser/findactiveuser",
        dataType: 'text',
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            
            $("a[href*='news']").css("display", "");
        	$("a[href*='games']").css("display", "");
        	$("a[href*='advertisements']").css("display", "");
        	$("a[href*='forum']").css("display", "");
        	$(".btn-primary").css("display","");
        	
        	if(JSON.parse(data).user_level < 4){
        		$("a[href*='registration']").css("display", "");
        		$("a[href*='login']").css("display", "");
        		$("a[onclick*='javascript: $.ajax({type: ']").css("display", "none");
        	}
        
        if(JSON.parse(data).user_level >= 4){
        	
        	$("a[href*='listofusers']").css("display", "");
       	 $("a[href*='profile']").css("display", "");
       	 $("a[onclick*='javascript: $.ajax({type: ']").css("display", "");
        }
            
            if(JSON.parse(data).user_level >= 4){
                $("#new_article").css("display", "inline");
           }
        },
        error: function (e) {
            console.log(e);
        }
    });

}

function deleteArticle(idas) {
	//var url = new URL(window.location.href);
	//var idas = url.searchParams.get("id");
	
	
	$.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/articles/removeArticle?id="+idas,
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log("Article removed");
            window.location.href = "/news";
        },
        error: function (e) {
            //console.log("bybis");

        }
    });
}

function searchArticles() {
	var tagsSearchQuery = $("#tagsSearchQuery").val();
	var categorySearchQuery = $("#categorySearchQuery").val();
	/*
	if (tagsSearchQuery == "" || tagsSearchQuery == null)
		tagsSearchQuery = "none";
	if (categorySearchQuery == "" || categorySearchQuery == null)
		categorySearchQuery = "none";
		*/
	console.log(tagsSearchQuery);
	console.log(categorySearchQuery);
	$.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/articles/findByTagsAndCategory?tagsQuery="+tagsSearchQuery+"&categoryQuery="+categorySearchQuery,
        cache: false,
        async: false,
        timeout: 600000,
        success: function (data) {
        	 $(".card-deck").html("");
        	 if (data.length == 0)
        		 {
        		 	alert("nėra atitinkančių naujienų");
        		 	return;
        		 }
            for (var i = 0; i < data.length; i++){
                var name = data[i].name;
                $(".card-deck").append('<div class="card mt-4" style="min-width: 20rem; "><img class="card-img-top" style="width: 100%; height: 15vw; object-fit: cover;" src="'+data[i].imageLink+'" alt="Card image cap"/> <div class="card-body d-flex flex-column"> <h4 class="card-title text-center">' +data[i].name + '</h4> <p class="card-text text-center text-secondary"> <span class="font-weight-bold">Kategorija</span> '+data[i].category[0].name+'<br> <span class="font-weight-bold">Atnaujinimo laikas: </span>'+ getDateFormat(data[i].update_date) +'<br> <span class="font-weight-bold">Žymės: </span>'+ data[i].tags +' </p> <a href="/article?id=' + data[i].id + '" class="btn btn-light mt-auto ">Skaityti plačiau...</a> </div> </div>');
                //'<div class="card mt-4" style="min-width: 20rem; "><img class="card-img-top" style="width: 100%; height: 15vw; object-fit: cover;"src="'+data[i].imageLink+'" alt="Card image cap"/> <div class="card-body d-flex flex-column"> <h4 class="card-title text-center">' +data[i].name + '</h4> <p class="card-text text-center text-secondary"> <span class="font-weight-bold">Kategorija</span> 'data[i].category[0].name'<br> <span class="font-weight-bold">Atnaujinimo laikas: </span>'+ data[i].update_date +'<br> <span class="font-weight-bold">Žymės: </span>'+ data[i].tags +' </p> <a href="/article?id=' + data[i].id + '" class="btn btn-light mt-auto ">Skaityti plačiau...</a> </div> </div>'
                //$(".card-deck").append('<div class="card" style="width: 18rem;"><img class="card-img-top" style="max-width:250px; max-height:300px; width: 100%; height: 100%; display: block; margin-left: auto; margin-right: auto;" src="'+data[i].imageLink+'" alt="Card image cap"/> <div class="card-body d-flex flex-column">  <h5 class="card-title text-center">'+data[i].name+'</h5><p class="card-text">Kategorija: '+data[i].category[0].name+'<br>Atnaujinimo laikas: '+data[i].update_date+'<br>Įvertinimas: '+data[i].rating+'<br>Žymės: '+data[i].tags+'</p><a href="/article?id='+data[i].id+'" class="btn btn-primary mt-auto">Daugiau...</a></div> </div>');

            }
        },
        error: function (e) {
            alert("nėra atitinkančių naujienų")

        }
    });
}

function sortArticlesByDate(){
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/articles/getArticlesSortedByDate",
        cache: false,
        timeout: 600000,
        success: function (data) {
        	console.log(data);
            $(".card-deck").html("");
            for (var i = 0; i < data.length; i++){
                $(".card-deck").append('<div class="card mt-4" style="min-width: 20rem; "><img class="card-img-top" style="width: 100%; height: 15vw; object-fit: cover;" src="'+data[i].imageLink+'" alt="Card image cap"/> <div class="card-body d-flex flex-column"> <h4 class="card-title text-center">' +data[i].name + '</h4> <p class="card-text text-center text-secondary"> <span class="font-weight-bold">Kategorija</span> '+data[i].category[0].name+'<br> <span class="font-weight-bold">Atnaujinimo laikas: </span>'+ getDateFormat(data[i].update_date) +'<br> <span class="font-weight-bold">Žymės: </span>'+ data[i].tags +' </p> <a href="/article?id=' + data[i].id + '" class="btn btn-light mt-auto ">Skaityti plačiau...</a> </div> </div>');
            }
        },
        error: function (e) {
            console.log("Error: " + e);
        }
    });
}

module.exports = {
		deleteArticle: deleteArticle,
		searchArticles: searchArticles,
		sortArticlesByDate: sortArticlesByDate,
};
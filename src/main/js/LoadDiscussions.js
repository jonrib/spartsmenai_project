$(document).ready(function () {

        fire_ajax_submit();


});

function fire_ajax_submit() {
	
	$.ajax({
		type : "GET",
		contentType : "application/json",
		accept: 'text/plain',
		url : "activeuser/findactiveuser",
		dataType: 'text',
		async: false,
		success : function(data) {
			 var active = JSON.parse(data);
			 var activeUserName = active.username;
			 var activeUserLevel = active.user_level;
			 
			 $("a[href*='news']").css("display", "");
	        	$("a[href*='games']").css("display", "");
	        	$("a[href*='advertisements']").css("display", "");
	        	$("a[href*='forum']").css("display", "");
	        	$(".btn-primary").css("display","");
	        	
	        	if(JSON.parse(data).user_level < 4){
	        		$("a[href*='registration']").css("display", "");
	        		$("a[href*='login']").css("display", "");
	        		$("a[onclick*='javascript: $.ajax({type: ']").css("display", "none");
	        	}
	        
	        if(JSON.parse(data).user_level >= 4){
	        	
	        	$("a[href*='listofusers']").css("display", "");
	       	 $("a[href*='profile']").css("display", "");
	       	 $("a[onclick*='javascript: $.ajax({type: ']").css("display", "");
	        }
			 
			 if (activeUserLevel < 4)
				 $('a[href *= editDiscussion]').hide();
			 
		},
		error : function(e) {
			alert("Error!")
			console.log("ERROR: ", e);
		}
	});
	
	

    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/forums",
        cache: false,
        timeout: 600000,
        success: function (data) {
            for (var i = 0; i < data.length; i++){
                $(".card-deck").append('<div class="card" style="width: 18rem;"> <div class="card-body d-flex flex-column">  <h5 class="card-title text-center">'+data[i].topic+'</h5><a href="/editDiscussion?id='+data[i].id+'" class="btn btn-primary mt-auto">Daugiau...</a><a href="javascript: ui.deleteDiscussion(\''+data[i].id+'\')" class="btn btn-danger mt-auto">Šalinti...</a></div> </div>');
            }

        },
        error: function (e) {
            console.log("Error: " + e);

        }
    });

}

function deleteDiscussion(idas) {
	//var url = new URL(window.location.href);
	//var idas = url.searchParams.get("id");
	
	
	$.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/forums/removeDiscussion?id="+idas,
        cache: false,
        timeout: 600000,
        success: function (data) {
            window.location.href = "/forum";
        },
        error: function (e) {
        }
    });
}

function searchDiscussions() {
	var searchQuery = $("#searchQuery").val();
	
	
	$.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/forums/findByName?query="+searchQuery,
        cache: false,
        async: false,
        timeout: 600000,
        success: function (data) {
        	 $(".card-deck").html("");
            for (var i = 0; i < data.length; i++){
                var name = data[i].name;
                $(".card-deck").append('<div class="card" style="width: 18rem;"> <div class="card-body d-flex flex-column">  <h5 class="card-title text-center">'+data[i].topic+'</h5><a href="/editDiscussion?id='+data[i].id+'" class="btn btn-primary mt-auto">Daugiau...</a><a href="javascript: ui.deleteDiscussion(\''+data[i].id+'\')" class="btn btn-danger mt-auto">Šalinti...</a></div> </div>');
            }
        },
        error: function (e) {
            //console.log("bybis");

        }
    });
}

module.exports = {
		deleteDiscussion: deleteDiscussion,
		searchDiscussions: searchDiscussions,
};
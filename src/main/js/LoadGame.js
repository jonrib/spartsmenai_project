$(document).ready(function () {
    console.log("here");
    var url = new URL(window.location.href);
    var id = url.searchParams.get("id");
    if (id != "" && id != null && typeof id != "undefined"){
        fire_ajax_submit(id);
    }
});

function getDateFormat(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    var date = new Date();
    date.toLocaleDateString();

    return [year, month, day].join('-');
};

function fire_ajax_submit(id) {
    var idas = "";
    $.ajax({
        type : "GET",
        contentType : "application/json",
        accept: 'text/plain',
        url : "gamess/findById?id="+id,
        dataType: 'text',
        async: false,
        success : function(data) {
            console.log(data);
            idas = JSON.parse(data).id;
            $(".game-data")
                .append('' +
                    '<tr><td><h1>Pavadinimas: '+JSON.parse(data).name+'</h1></td></tr>' +
                    '<tr><td align="center"><img style="max-height:400px; width:100%; object-fit: cover;" id="imageLinkas" src="'+JSON.parse(data).photo+'" alt="Nuotrauka"/></td></tr>' +
                    '<tr><td><p><span class="font-weight-bold">Aprasymas: </span>'+JSON.parse(data).description+'</p> </td></tr>' +
                    '<tr><td><p><span class="font-weight-bold">Vieta: </span>'+JSON.parse(data).place+'</p> </td></tr>'+
                    '<tr><td><p><span class="font-weight-bold">Komanda A: </span>'+JSON.parse(data).team_a+'</p> </td></tr>'+
                    '<tr><td><p><span class="font-weight-bold">Komanda B: </span>'+JSON.parse(data).team_b+'</p> </td></tr>'+
                    '<tr><td><p><span class="font-weight-bold">Komandos ' +JSON.parse(data).team_a+ ' rezultatas: </span>'+JSON.parse(data).result_a+' </p> </td></tr>'+
                    '<tr><td><p><span class="font-weight-bold">Komandos ' +JSON.parse(data).team_b+ ' rezultatas: </span>'+JSON.parse(data).result_b+'</p> </td></tr>'+
                    '<tr><td><p><span class="font-weight-bold">Veiksmo data: </span>'+getDateFormat(JSON.parse(data).event_date)+'</p>  </td></tr>');   
            },
  
        error: function (e) {
            console.log("error");
        },
        
        complete: function(data) {
                        
                        //idas = JSON.parse(data).id;
            $.ajax({
                type: "GET",
                contentType: "application/json",
                accept: 'text/plain',
                url: "/activeuser/findactiveuser",
                dataType: 'text',
                cache: false,
                timeout: 600000,
                success: function (data) {
                    console.log(data); 
                    
                    $("a[href*='news']").css("display", "");
                	$("a[href*='games']").css("display", "");
                	$("a[href*='advertisements']").css("display", "");
                	$("a[href*='forum']").css("display", "");
                	$(".btn-primary").css("display","");
                	
                	if(JSON.parse(data).user_level < 4){
                		$("a[href*='registration']").css("display", "");
                		$("a[href*='login']").css("display", "");
                		$("a[onclick*='javascript: $.ajax({type: ']").css("display", "none");
                	}
                
                if(JSON.parse(data).user_level >= 4){
                	
                	$("a[href*='listofusers']").css("display", "");
               	 $("a[href*='profile']").css("display", "");
               	 $("a[onclick*='javascript: $.ajax({type: ']").css("display", "");
                }
                
                
                if(JSON.parse(data).user_level === 4){
                        $(".game-buttons")
                        .append('' +
                            '<a href="https://www.facebook.com/sharer.php?u='+window.location.href+'" target="_blank" ' +
                            'onclick="window.open(this.href,\'targetWindow\',\'toolbar=no,location=0,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=250\'); return false">' +
                            '<button class="btn btn-facebook mr-3"> Dalintis Facebook </button> </a>')
                        
                }
            
                if(JSON.parse(data).user_level === 5){
                    $("#new_game").css("display", "inline");
                    $(".game-buttons").html("")
                    $(".game-buttons")
                        .append('' +
                            '<a href="/editGame?id='+idas+'" class="btn btn-primary mt-auto mr-3">Redaguoti</a>' +
                            '<a href="/editGameResult?id='+idas+'" class="btn btn-primary mt-auto mr-3">Redaguoti Rezultatą</a>' +
                            '<a href="/addMembertoGame?id='+idas+'" class="btn btn-primary mt-auto mr-3">Varžybų nariai</a>' +
                            '<a href="https://www.facebook.com/sharer.php?u='+window.location.href+'" target="_blank" ' +
                            'onclick="window.open(this.href,\'targetWindow\',\'toolbar=no,location=0,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=250\'); return false">' +
                            '<button class="btn btn-facebook mr-3"> Dalintis Facebook </button> </a>')
                }
                if(JSON.parse(data).user_level > 5){  
                    $(".game-buttons").html("")
                    $(".game-buttons")
                        .append('' +
                            '<a href="/editGame?id='+idas+'" class="btn btn-primary mt-auto mr-3">Redaguoti</a>' +
                            '<a href="/editGameResult?id='+idas+'" class="btn btn-primary mt-auto mr-3">Redaguoti Rezultatą</a>' +
                            '<a href="javascript: ui.deleteGame(\''+JSON.parse(data).id+'\')" class="btn btn-danger mt-auto mr-3">Šalinti</a>'+
                            '<a href="https://www.facebook.com/sharer.php?u='+window.location.href+'" target="_blank" ' +
                            'onclick="window.open(this.href,\'targetWindow\',\'toolbar=no,location=0,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=250\'); return false">' +
                            '<button class="btn btn-facebook mr-3"> Dalintis Facebook </button> </a>')
                }
                             
                            },error: function (e) {
                                console.log("error");
                            }
                });
            }
        });
        
    }

function deleteGame(idas) {
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/gamess/removeGame?id="+idas,
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log("Game removed");
            window.location.href = "/games";
        },
        error: function (e) {
            console.log("Error: " + e);
        }
    });
}

module.exports = {
    deleteGame: deleteGame,
}
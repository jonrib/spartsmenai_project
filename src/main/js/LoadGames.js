$(document).ready(function() {
  fire_ajax_submit();
});



function fire_ajax_submit() {
  $.ajax({
    type: "GET",
    contentType: "application/json",
    url: "/gamess",
    cache: true,
    timeout: 600000,
    success: function(data) {
      console.log(data);
      for (var i = 0; i < data.length; i++) {
        $(".card-deck").append(
            '<div class="card mt-4" style="min-width: 20rem; "><img class="card-img-top" style="width: 100%; height: 15vw; object-fit: cover;"src="'+data[i].photo+'" alt="Card image cap"/> <div class="card-body d-flex flex-column"> <h4 class="card-title text-center">' +data[i].name + '</h4> <p class="card-text text-center text-secondary"> <span class="font-weight-bold">'+ data[i].team_a +'</span> prieš <span class="font-weight-bold"> '+ data[i].team_b +' </span> <br> data: '+ getDateFormat(data[i].event_date) +' </p> <a href="/openGame?id=' + data[i].id + '" class="btn btn-light mt-auto ">Skaityti plačiau...</a> </div> </div>'
        );
      }
    },
    error: function(e) {
      alert(e);
    }
  });

  $.ajax({
    type: "GET",
    contentType: "application/json",
    accept: 'text/plain',
    url: "/activeuser/findactiveuser",
    dataType: 'text',
    cache: false,
    timeout: 600000,
    success: function (data) {
        console.log(data); 
        
        $("a[href*='news']").css("display", "");
    	$("a[href*='games']").css("display", "");
    	$("a[href*='advertisements']").css("display", "");
    	$("a[href*='forum']").css("display", "");
    	$(".btn-primary").css("display","");
    	
    	if(JSON.parse(data).user_level < 4){
    		$("a[href*='registration']").css("display", "");
    		$("a[href*='login']").css("display", "");
    		$("a[onclick*='javascript: $.ajax({type: ']").css("display", "none");
    	}
    
    if(JSON.parse(data).user_level >= 4){
    	
    	$("a[href*='listofusers']").css("display", "");
   	 $("a[href*='profile']").css("display", "");
   	 $("a[onclick*='javascript: $.ajax({type: ']").css("display", "");
    }

     if(JSON.parse(data).user_level >= 5){
        $("#new_game").css("display", "inline");
   }
     
    },
    error: function (e) {
        alert(e);
    }
});

}

function sortGamesByEventDateAsc(){
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/gamess/getGamesSortedByEventDateAsc",
        cache: false,
        timeout: 600000,
        success: function (data) {
            $(".card-deck").html("");
            for (var i = 0; i < data.length; i++){
                $(".card-deck").append(
                    '<div class="card mt-4" style="min-width: 20rem; "><img class="card-img-top" style="width: 100%; height: 15vw; object-fit: cover;"src="'+data[i].photo+'" alt="Card image cap"/> <div class="card-body d-flex flex-column"> <h4 class="card-title text-center">' +data[i].name + '</h4> <p class="card-text text-center text-secondary"> <span class="font-weight-bold">'+ data[i].team_a +'</span> prieš <span class="font-weight-bold"> '+ data[i].team_b +' </span> <br> data: '+ getDateFormat(data[i].event_date) +' </p> <a href="/openGame?id=' + data[i].id + '" class="btn btn-primary mt-auto">Daugiau...</a> </div> </div>'
                );
            }
        },
        error: function (e) {
            console.log("Error: " + e);
        }
    });
}

function sortGamesByTitleAsc(){
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/gamess/getGamesSortedByTitleAsc",
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            $(".card-deck").html("");
            for (var i = 0; i < data.length; i++){
                $(".card-deck").append(
                    '<div class="card mt-4" style="min-width: 20rem; "><img class="card-img-top" style="width: 100%; height: 15vw; object-fit: cover;"src="'+data[i].photo+'" alt="Card image cap"/> <div class="card-body d-flex flex-column"> <h4 class="card-title text-center">' +data[i].name + '</h4> <p class="card-text text-center text-secondary"> <span class="font-weight-bold">'+ data[i].team_a +'</span> prieš <span class="font-weight-bold"> '+ data[i].team_b +' </span> <br> data: '+ getDateFormat(data[i].event_date) +' </p> <a href="/openGame?id=' + data[i].id + '" class="btn btn-primary mt-auto">Daugiau...</a> </div> </div>'
                );
            }
        },
        error: function (e) {
            console.log("Error: " + e);
        }
    });
}

function searchGames() {
    var categorySearchQuery = $("#categorySearchQuery").val();
	$.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/gamess/findByCategory?&categoryQuery="+categorySearchQuery,
        cache: false,
        async: false,
        timeout: 600000,
        success: function (data) {
            if(data.length > 0) {
        	 $(".card-deck").html("");
            for (var i = 0; i < data.length; i++){
                var name = data[i].name;
                $(".card-deck").append(
                    '<div class="card mt-4" style="min-width: 20rem; "><img class="card-img-top" style="width: 100%; height: 15vw; object-fit: cover;"src="'+data[i].photo+'" alt="Card image cap"/> <div class="card-body d-flex flex-column"> <h4 class="card-title text-center">' +data[i].name + '</h4> <p class="card-text text-center text-secondary"> <span class="font-weight-bold">'+ data[i].team_a +'</span> prieš <span class="font-weight-bold"> '+ data[i].team_b +' </span> <br> data: '+ getDateFormat(data[i].event_date) +' </p> <a href="/openGame?id=' + data[i].id + '" class="btn btn-primary mt-auto">Daugiau...</a> </div> </div>'
                );
            }} else alert("Varžybų su tokia kategorija nebuvo rasta");
        },
        error: function (e) {
           alert(e)

        }
    });
}

function getDateFormat(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    var date = new Date();
    date.toLocaleDateString();

    return [year, month, day].join('-');
};

module.exports = {
    sortGamesByEventDateAsc: sortGamesByEventDateAsc,
    sortGamesByTitleAsc: sortGamesByTitleAsc,
    searchGames: searchGames,
};

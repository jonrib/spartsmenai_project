$(document).ready(function () {

	var url = new URL(window.location.href);
	var id = url.searchParams.get("id");
	if (id != "" && id != null && typeof id != "undefined"){
		loadArticle(id);
	}


});

function getDateFormat(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
    	hours = d.getHours(),
    	minutes = d.getMinutes();
    
    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    if (hours < 10)
    	hours = '0' + hours;
    if (minutes < 10)
    	minutes = '0' + minutes;
    var date = new Date();
    date.toLocaleDateString();

    return [year, month, day].join('-') + " " + [hours, minutes].join(':');
};

function loadArticle(id) {
    var idas = "";
    $.ajax({
        type: "GET",
        contentType: "application/json",
        accept: 'text/plain',
        url: "articles/findById?id="+id,
        cache: false,
        timeout: 600000,
        dataType: 'text',
		async: false,
        success: function (data) {
        	console.log(JSON.parse(data).name);
            idas = JSON.parse(data).id;
        	//$(".card-deck").append('<div class="card" style="width: 18rem;"><img class="card-img-top" style="max-width:800px; max-height:300px; width: 100%; height: 100%; display: block; margin-left: auto; margin-right: auto;" src="'+JSON.parse(data).imageLink+'" alt="Card image cap"/><div class="card-body d-flex flex-column"><h5 class="card-title text-center">'+JSON.parse(data).name+'</h5><p class="card-text">'+JSON.parse(data).content+'</p><p class="card-text"><a href="'+JSON.parse(data).sourceLink+'">Šaltinis</a></p><p class="card-text">Kategorija: '+JSON.parse(data).category[0].name+'</p><p class="card-text">Žymės: '+JSON.parse(data).tags+'</p><p class="card-text">Sukūrimo data: '+JSON.parse(data).creation_date+'</p><p class="card-text">Atnaujinimo data: '+JSON.parse(data).update_date+'</p><p class="card-text">Įvertinimas: '+JSON.parse(data).rating+'</p><a href="/editArticle?id='+JSON.parse(data).id+'" class="btn btn-primary mt-auto">Redaguoti</a><a href="javascript: ui.deleteArticle(\''+JSON.parse(data).id+'\')" class="btn btn-danger mt-auto">Šalinti...</a></div></div>');
        	//<div class="dropdown"><button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Įvertinimas</button><div class="dropdown-menu" aria-labelledby="dropdownMenuButton"><a class="dropdown-item" href="/rateArticle?id='+JSON.parse(data).id+'&rating=1">1</a><a class="dropdown-item" href="/rateArticle?id='+JSON.parse(data).id+'&rating=2">2</a><a class="dropdown-item" href="/rateArticle?id='+JSON.parse(data).id+'&rating=3">3</a><a class="dropdown-item" href="/rateArticle?id='+JSON.parse(data).id+'&rating=4">4</a><a class="dropdown-item" href="/rateArticle?id='+JSON.parse(data).id+'&rating=5">5</a></div></div>
        	$(".article-data")
            .append('' +
                '<tr><td><h1>'+JSON.parse(data).name+'</h1></td></tr>' +
                '<tr><td align= "center"><img style="max-height:400px; width:100%; object-fit: cover;"id="imageLinkas" src="'+JSON.parse(data).imageLink+'" alt="Nuotrauka"/></td></tr>' +
                '<tr><td><p>'+JSON.parse(data).content+'</p> </td></tr>' +
                '<tr><td><p><span class="font-weight-bold">Kategorija: </span>'+JSON.parse(data).category[0].name+'</p> </td></tr>'+
                '<tr><td><p><span class="font-weight-bold">Žymės: </span>'+JSON.parse(data).tags+'</p> </td></tr>'+
                '<tr><td><p><span class="font-weight-bold">Sukūrimo data: </span>'+getDateFormat(JSON.parse(data).creation_date)+'</p> </td></tr>'+
                '<tr><td><p><span class="font-weight-bold">Atnaujinimo data: </span>'+getDateFormat(JSON.parse(data).update_date)+'</p> </td></tr>'+
                '<tr><td><p><a href="'+JSON.parse(data).sourceLink+'">Šaltinis</a></p> </td</tr>');
        },  
        error: function (e) {

        }
    });
    
    $.ajax({
        type: "GET",
        contentType: "application/json",
        accept: 'text/plain',
        url: "/activeuser/findactiveuser",
        dataType: 'text',
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data); 
            
            $("a[href*='news']").css("display", "");
        	$("a[href*='games']").css("display", "");
        	$("a[href*='advertisements']").css("display", "");
        	$("a[href*='forum']").css("display", "");
        	$(".btn-primary").css("display","");
        	
        	if(JSON.parse(data).user_level < 4){
        		$("a[href*='registration']").css("display", "");
        		$("a[href*='login']").css("display", "");
        		$("a[onclick*='javascript: $.ajax({type: ']").css("display", "none");
        	}
        
        if(JSON.parse(data).user_level >= 4){
        	
        	$("a[href*='listofusers']").css("display", "");
       	 $("a[href*='profile']").css("display", "");
       	 $("a[onclick*='javascript: $.ajax({type: ']").css("display", "");
        }
            
            if(JSON.parse(data).user_level >= 4){
            	$(".article-buttons")
                .append('' +
                    '<a href="/editArticle?id='+idas+'" class="btn btn-primary mt-auto mr-3">Redaguoti</a>');
            }
            if(JSON.parse(data).user_level > 5){
            	$(".article-buttons")
                .append('' +
                    '<a href="javascript: ui.deleteArticle(\''+idas+'\')" class="btn btn-danger mt-auto mr-3">Šalinti</a>');
            }
        },
        error: function (e) {
            console.log(e);
        }
    });

}

function deleteArticle(idas) {
	//var url = new URL(window.location.href);
	//var idas = url.searchParams.get("id");
	
	
	$.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/articles/removeArticle?id="+idas,
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log("Article removed");
            window.location.href = "/news";
        },
        error: function (e) {
            //console.log("bybis");

        }
    });
}


module.exports = {
		deleteArticle: deleteArticle,
};
$(document).ready(function () {

        fire_ajax_submit();


});

function fire_ajax_submit() {
	
	var url = new URL(window.location.href);
	var id = url.searchParams.get("id");
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/users/findById?id="+ id ,
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            	if(data.user_level == 4)
                $("#detaileduser").append('<tr><td>Vartotojo vardas:</td><td>'+data.username+
                		'</td></tr><tr><td>Naudotojo emailas</td><td>'+data.email+
                		'</td></tr><tr><td>Naudotojo Telefono numeris</td><td>'+data.Telnr+
                		'</td></tr>');
            	if(data.user_level == 5)
            		$("#detaileduser").append('<tr><td>Organizacijos atstovo vardas:</td><td>'+data.username+
                    		'</td></tr><tr><td>Organizacijos atstovo emailas</td><td>'+data.email+
                    		'</td></tr><tr><td>Organizacijos atstovo Telefono numeris</td><td>'+data.Telnr+
                    		'</td></tr><tr><td>Organizacijos pavadinimas</td><td>'+data.organizacpav+
                    		'</td></tr><tr><td>Organizacijos adresas</td><td>'+data.organizacadr+
                    		'</td></tr>');
        },
        error: function (e) {
            //console.log("bybis");

        }
    });
    
    $.ajax({
        type: "GET",
        contentType: "application/json",
        accept: 'text/plain',
        url: "/activeuser/findactiveuser",
        dataType: 'text',
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            
            $("a[href*='news']").css("display", "");
        	$("a[href*='games']").css("display", "");
        	$("a[href*='advertisements']").css("display", "");
        	$("a[href*='forum']").css("display", "");
        	$(".btn-primary").css("display","");
        	
        	if(JSON.parse(data).user_level < 4){
        		$("a[href*='registration']").css("display", "");
        		$("a[href*='login']").css("display", "");
        		$("a[onclick*='javascript: $.ajax({type: ']").css("display", "none");
        	}
        
        if(JSON.parse(data).user_level >= 4){
        	
        	$("a[href*='listofusers']").css("display", "");
       	 $("a[href*='profile']").css("display", "");
       	 $("a[onclick*='javascript: $.ajax({type: ']").css("display", "");
        }
        },
        error: function (e) {
            console.log(e);
        }
    });

}

function deleteArticle(idas) {
	//var url = new URL(window.location.href);
	//var idas = url.searchParams.get("id");
	
	
	$.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/articles/removeArticle?id="+idas,
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log("Article removed");
            window.location.href = "/news";
        },
        error: function (e) {
            //console.log("bybis");

        }
    });
}

function searchArticles() {
	var searchQuery = $("#searchQuery").val();
	
	
	$.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/articles/findByName?query="+searchQuery,
        cache: false,
        async: false,
        timeout: 600000,
        success: function (data) {
        	 $(".card-deck").html("");
            for (var i = 0; i < data.length; i++){
                var name = data[i].name;
                $(".card-deck").append('<div class="card" style="width: 18rem;"> <div class="card-body d-flex flex-column"><h5 class="card-title text-center">'+data[i].name+'</h5><a href="/editArticle?id='+data[i].id+'" class="btn btn-primary mt-auto">Daugiau...</a><a href="javascript: ui.deleteArticle(\''+data[i].id+'\')" class="btn btn-danger mt-auto">Šalinti...</a></div> </div>');
            }
        },
        error: function (e) {
            //console.log("bybis");

        }
    });
}

module.exports = {
		deleteArticle: deleteArticle,
		searchArticles: searchArticles,
};
$(document).ready(function () {

        fire_ajax_submit();


});

function fire_ajax_submit() {

    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/users",
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            //$(".card-deck").append(' <table class="table table-hover"><thead><tr><th scope="col">Vardas</th><th scope="col">Pavarde</th></tr></thead><tbody>');
            for (var i = 0; i < data.length; i++){
               // var name = data[i].name;
                $("#tbodyUsr").append('<tr><td><a href="/detailedprofile?id=' + data[i].id + '">'+data[i].username+'</a></td><td>'+data[i].email+'</td><td><a style="display: none;" href="javascript: ui.deleteUser(\''+data[i].id+'\')" class="btn btn-danger mt-auto">Šalinti Vartotoja</a> </td></tr>');
            }  
        },
        error: function (e) {
          
        }
    });
    
    

    	$.ajax({
    	       type: "GET",
    	       contentType: "application/json",
    	       accept: 'text/plain',
    	       url: "/activeuser/findactiveuser",
    	       dataType: 'text',
    	       cache: false,
    	       timeout: 600000,
    	       success: function (data) {
    	         

    	    	   $("a[href*='news']").css("display", "");
    	        	$("a[href*='games']").css("display", "");
    	        	$("a[href*='advertisements']").css("display", "");
    	        	$("a[href*='forum']").css("display", "");
    	        	$(".btn-primary").css("display","");
    	        	
    	        	if(JSON.parse(data).user_level < 4){
    	        		$("a[href*='registration']").css("display", "");
    	        		$("a[href*='login']").css("display", "");
    	        		$("a[onclick*='javascript: $.ajax({type: ']").css("display", "none");
    	        	}
    	        
    	        if(JSON.parse(data).user_level >= 4){
    	        	
    	        	$("a[href*='listofusers']").css("display", "");
    	       	 $("a[href*='profile']").css("display", "");
    	       	 $("a[onclick*='javascript: $.ajax({type: ']").css("display", "");
    	        }
               	
    	        if(JSON.parse(data).user_level === 4 ){
    	        	 
    	        	 $( ".btn-danger" ).css("display", "none");
    	        }
    	        
    	        //if admin
    	        if(JSON.parse(data).user_level > 5){
    	        	 $( ".btn-danger" ).css("display", "");
    	        }
    	        
    	       },
    	       error: function (e) {
    	       }
    	   });
    	
    
}

function deleteUser(idas) {
	//var url = new URL(window.location.href);
	//var idas = url.searchParams.get("id");
	
	
	$.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/users/removeUser?id="+idas,
        cache: false,
        timeout: 600000,
        success: function (data) {
        	alert("Naudotojas pašalintas sėkmingai")
            console.log("user removed");
            window.location.href = "/listofusers";
        },
        error: function (e) {
        	alert("Naudotojas pa6alintas nesėkmingai")

        }
    });
}

function searchUsers() {
	var searchQuery = $("#searchname").val();
	
	console.log("ahhhhh");
	$.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/users/findByName?query="+searchQuery,
        cache: false,
        async: false,
        timeout: 600000,
        success: function (data) {
        	 $("#tbodyUsr").html("");
        	 for (var i = 0; i < data.length; i++){
                 console.log(data[i].username);
                  $("#tbodyUsr").append('<tr><td><a href="/detailedprofile?id=' + data[i].id + '">'+data[i].username+'</a></td><td>'+data[i].email+'</td><td><a href="javascript: ui.deleteUser(\''+data[i].id+'\')" class="btn btn-danger mt-auto">Šalinti Vartotoja</a> </td></tr>');
              }
        },
        error: function (e) {
        

        }
    });
	
	
	$.ajax({
	       type: "GET",
	       contentType: "application/json",
	       accept: 'text/plain',
	       url: "/activeuser/findactiveuser",
	       dataType: 'text',
	       cache: false,
	       timeout: 600000,
	       success: function (data) {
	           console.log(data); 
	           
	        if(JSON.parse(data).user_level >= 4 && JSON.parse(data).user_level <= 5){
	        	
	        	 //del mygtukai
	        	 $( ".btn-danger" ).css("display", "none");
	        }
	        //if admin
	        if(JSON.parse(data).user_level > 5){
	        	 $( ".btn-danger" ).css("display", "block");
	        }
	        
	       },
	       error: function (e) {
	           
	       }
	   });
}

module.exports = {
		deleteUser: deleteUser,
		searchUsers: searchUsers,
};
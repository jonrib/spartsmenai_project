var path = require('path');
const merge = require('webpack-merge');
var node_dir = __dirname + '/node_modules';
const TARGET = process.env.npm_lifecycle_event;

const common = {
    entry: {
    	HomePage : './src/main/js/HomePage.js', 
    	LoadArticles: "./src/main/js/LoadArticles.js",
    	ArticleEditorHandler: "./src/main/js/ArticleEditorHandler.js", 
    	LoadDiscussions: "./src/main/js/LoadDiscussions.js",
    	DiscussionEditorHandler: "./src/main/js/DiscussionEditorHandler.js", 
    	UserEditorHandler: "./src/main/js/UserEditorHandler.js",
    	LoadUsers: "./src/main/js/LoadUsers.js", 
        LoginHandler: "./src/main/js/LoginHandler.js", 
        LoadAdverts: "./src/main/js/LoadAdverts.js",
		LoadAdvert: "./src/main/js/LoadAdvert.js",
        LoadSpecificUser: "./src/main/js/LoadSpecificUser.js",
        AdvertEditorHandler: "./src/main/js/AdvertEditorHandler.js",
        LoadSpecificArticle: "./src/main/js/LoadSpecificArticle.js",
        ProfileHandler: "./src/main/js/ProfileHandler.js",
        GameEditorHandler: "./src/main/js/GameEditorHandler.js",
        LoadGames: "./src/main/js/LoadGames.js",
        LoadGame: "./src/main/js/LoadGame.js",
        LoadPendingUsers: "./src/main/js/LoadPendingUsers.js",},
    devtool: 'sourcemaps',
    cache: true,
    debug: true,
    output: {
        path: __dirname,
        filename: './src/main/resources/static/[name].js',
        libraryTarget: 'var',
        library: 'ui'
    },
    module: {
        rules: [
            {
            "include": __dirname + "/app/",
    test: /\.css$/,
    use: [
    { loader: "style-loader" },
    { loader: "css-loader" }
    ]
}
],
        loaders: [
            {
                test: path.join(__dirname, '.'),
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    cacheDirectory: true,
                    presets: ['es2015', 'react']
                }

            },
            {
            test: /\.(jpe?g|png|gif|svg)$/i,
             loaders: [
    'file?hash=sha512&digest=hex&name=[hash].[ext]',
    'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
    ]
},
{
test: /\.css$/,
loader: 'style-loader!css-loader'
},
{
test: /\.(woff|woff2|eot|ttf|svg)$/,
loader: 'url-loader?limit=100000'
},
        ]
    }
};

if (TARGET === 'start' || !TARGET) {
    module.exports = merge(common, {
        devServer: {
            port: 9090,
            proxy: {
                '/': {
                    target: 'http://localhost:8080',
                    secure: false,
                    prependPath: false
                }
            },
            publicPath: 'http://localhost:9090/',
            historyApiFallback: true
        },
        devtool: 'source-map'
    });
}

if (TARGET === 'build') {
    module.exports = merge(common, {});
}